#!/bin/sh

cat ../Data/run_*_household_size.dat > household_size.dat
echo "'household size ok!'"

cat ../Data/run_*_male_age_distribution.dat > male_age_gender_distribution.dat
echo "'male age gender distribution'"

cat ../Data/run_*_female_age_distribution.dat > female_age_gender_distribution.dat
echo "'female age gender distribution'"

grep 'single' ../Data/run_*_population_profile.dat | awk -F ':' '{print $3}'|sed 's/\t//g' > percentage_of_single.dat
echo "'single ok!'" 

grep 'married' ../Data/run_*_population_profile.dat | awk -F ':' '{print $3}'|sed 's/\t//g' > percentage_of_married.dat
echo "'married ok!'" 

grep 'separated' ../Data/run_*_population_profile.dat | awk -F ':' '{print $3}'|sed 's/\t//g' > percentage_of_separated.dat
echo "'separated ok!' "

# Rscript ../Data/general_analysis.sh

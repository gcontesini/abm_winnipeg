number_of_runs_int_ <- system(
  "ls ../Data/run_*_work_group_size.dat | wc -l",
  intern = TRUE
)

number_of_runs_int <- as.numeric( number_of_runs_int_ )
number_of_rows <- 40
work_data_mtx <- matrix( nrow = number_of_rows, ncol = number_of_runs_int ) # A kind of magic 
# work_data_mtx <- as.data.frame(work_data_mtx_)

for( column_index_int in 1:number_of_runs_int )
{
  work_data_file <- read.csv(
    paste(
      '../Data/run_',
      column_index_int,
      '_work_group_size.dat',
      sep=''
    ),
    header=TRUE
  )
  
  work_temp_hist <- hist(
    as.numeric(work_data_file$Group_size),
    breaks = seq(
      1,
      number_of_rows,
      1
    ),
    right = FALSE,
    plot = FALSE
  )
  sum_over_counts_int <- sum(work_temp_hist$counts)
  for( row_index_int in 1:number_of_rows)
  {
    work_data_mtx[work_temp_hist$breaks[row_index_int],column_index_int] <-
    work_temp_hist$counts[row_index_int] * 100 / sum_over_counts_int
  }
}

work_prob_mtx <- matrix( nrow = number_of_rows, ncol = 4 )

for( row_index_int in 1:number_of_rows )
{
  work_prob_mtx[row_index_int,1] <- mean( work_data_mtx[row_index_int,] )
  work_prob_mtx[row_index_int,2] <- sd( work_data_mtx[row_index_int,] )
  work_prob_mtx[row_index_int,3] <- min( work_data_mtx[row_index_int,] )
  work_prob_mtx[row_index_int,4] <- max( work_data_mtx[row_index_int,] )
}

non_zero_values <- which(work_prob_mtx[,1]!=0)

pdf("large_data_work_group_size.pdf")

work_barplot <- barplot(
  work_prob_mtx[c(min(non_zero_values):max(non_zero_values)),1],  
  main = "Average work Group Size\n Distribution",
  xlab = "Group Size",
  ylab = "Percentage",
  ylim = c(0,60),
  beside = TRUE,
  col = "darkgreen",
)

axis(
  1,
  at = work_barplot[,1],
  label = seq(
    min(which(work_prob_mtx[,1]!=0)),
    max(which(work_prob_mtx[,1]!=0)),
    1
  )
)

legend(
  18,
  58,
  "simulated",
  fill ="darkgreen"
)

box()

dev.off()
number_of_runs_int_ <- system(
  "ls ../Data/run_*_leisure_activity_group_size.dat | wc -l",
  intern = TRUE
)

number_of_runs_int <- as.numeric( number_of_runs_int_ )
number_of_rows <- 40
leisure_activity_data_mtx <- matrix( nrow = number_of_rows, ncol = number_of_runs_int ) # A kind of magic 
# leisure_activity_data_mtx <- as.data.frame(leisure_activity_data_mtx_)

for( column_index_int in 1:number_of_runs_int )
{
  leisure_activity_data_file <- read.csv(
    paste(
      '../Data/run_',
      column_index_int,
      '_leisure_activity_group_size.dat',
      sep=''
    ),
    header=TRUE
  )
  
  leisure_activity_temp_hist <- hist(
    as.numeric(leisure_activity_data_file$Group_size),
    breaks = seq(
      1,
      number_of_rows,
      1
    ),
    right = FALSE,
    plot = FALSE
  )
  sum_over_counts_int <- sum(leisure_activity_temp_hist$counts)
  for( row_index_int in 1:length(leisure_activity_temp_hist$counts))
  {
    leisure_activity_data_mtx[leisure_activity_temp_hist$breaks[row_index_int],column_index_int] <-
    leisure_activity_temp_hist$counts[row_index_int] * 100 / sum_over_counts_int
  }
}

leisure_activity_prob_mtx <- matrix( nrow = number_of_rows, ncol = 4 )

for( row_index_int in 1:number_of_rows )
{
  leisure_activity_prob_mtx[row_index_int,1] <- mean( leisure_activity_data_mtx[row_index_int,] )
  leisure_activity_prob_mtx[row_index_int,2] <- sd( leisure_activity_data_mtx[row_index_int,] )
  leisure_activity_prob_mtx[row_index_int,3] <- min( leisure_activity_data_mtx[row_index_int,] )
  leisure_activity_prob_mtx[row_index_int,4] <- max( leisure_activity_data_mtx[row_index_int,] )
}

pdf("large_data_leisure_activity_group_size.pdf")

leisure_activity_barplot <- barplot(
  leisure_activity_prob_mtx[c(which(leisure_activity_prob_mtx[,1]!=0)),1],  
  main = "Average Leisure-Activity Group Size\n Distribution",
  xlab = "Group Size",
  ylab = "Percentage",
  ylim = c(0,25),
  beside = TRUE,
  col = "darkgreen",
)

axis(
  1,
  at = leisure_activity_barplot[,1],
  label = seq(
    min(which(leisure_activity_prob_mtx[,1]!=0))+1,
    max(which(leisure_activity_prob_mtx[,1]!=0)),
    1
  )
)

legend(
  34,
  24,
  "simulated",
  fill ="darkgreen"
)

box()

dev.off()
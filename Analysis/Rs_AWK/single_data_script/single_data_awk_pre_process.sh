#!/bin/bash

#################################################### Code for short simulation report

# ------------------------------------------------------ Numero de Casos por dia 

awk -F : 'NR==1{print $1":"$2":"$3":"$4":"$5} $3!="None" {print $1":"$2":"$3":"$4":"$5}' ./../../Data/*_1_simulation_report* > single_1_data_number_of_cases_per_day_1.dat
awk -F : 'NR==1{print $1 ":" $2 ":" $3 ":" $4 ":" $5} $4!="initial_case" {print $1 ":" $2 ":" $3 ":" $4 ":" $5}' single_1_data_number_of_cases_per_day_1.dat > single_1_data_number_of_cases_per_day_2.dat
awk -F : '!x[$1]++ {print $1 ":" $2 ":" $3 ":"$4 ":" $5}' single_1_data_number_of_cases_per_day_2.dat > single_1_data_number_of_cases_per_day_3.dat

Rscript ./single_data_number_of_cases_per_day.R

# ------------------------------------------------------ Numero de Diagnosticos por dia

awk -F : 'NR==1 {print $1":"$2":"$3":"$5} $3!="None" {print $1":"$2":"$3":"$5}' ./../../Data/*_1_simulation_report* > single_1_data_number_of_diagnosed_1.dat
awk -F : 'NR==1 {print $1":"$2":"$3":"$4} $2=="True" {print $1":"$2":"$3":"$4}' single_1_data_number_of_diagnosed_1.dat > single_1_data_number_of_diagnosed_2.dat
awk -F : '!x[$1]++ {print $1":"$4}' single_1_data_number_of_diagnosed_2.dat > single_1_data_number_of_diagnosed_3.dat

Rscript ./single_data_number_of_diagnoses_per_day.R


rm single_1_data_number_of_diagnosed_1.dat
rm single_1_data_number_of_diagnosed_2.dat
rm single_1_data_number_of_diagnosed_3.dat

rm single_1_data_number_of_cases_per_day_1.dat
rm single_1_data_number_of_cases_per_day_2.dat
rm single_1_data_number_of_cases_per_day_3.dat

# ------------------------------------------------------ Numero Total de infectados

# awk -F : '$12!="None" {print $1":"$2":"$3":"$5}' ./../Data/run_1_simulation_report.dat > single_1_data_number_of_infected_1.dat
# awk -F : '!x[$1]++ {print $1":"$2":"$3":"$4}' single_1_data_number_of_infected_1.dat > single_1_data_number_of_infected_2.dat

# ----------------------------------------------------- Demographic Distributions

# awk -F : '!x[$1]++ {print $1":"$2":"$5}' ./../Data/run_1_simulation_report.dat > single_1_data_demographic_distributions.dat
# sed -i '1d' single_1_data_demographic_distributions.dat 

# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Populacao total

# echo "Total Population:"
# cat single_1_data_demographic_distributions.dat | wc -l
# echo "Number of Diagnosed Cases:"
# cat single_1_data_number_of_diagnosed_3.dat | wc -l
# echo "Number of infected:"
# cat single_1_data_number_of_infected_2.dat | wc -l


#################################################### Code for full simulation report
#|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

# # ------------------------------------------------------ Numero de Casos por dia 

# awk -F : 'NR==1{print $1":"$11":"$12":"$15":"$18} $12!="None" {print $1":"$11":"$12":"$15":"$18}' ./../Data/run_1_simulation_report.dat > single_1_data_number_of_cases_per_day_1.dat
# awk -F : 'NR==1{print $1 ":" $2 ":" $3 ":" $4 ":" $5} $4!="initial_case" {print $1 ":" $2 ":" $3 ":" $4 ":" $5}' single_1_data_number_of_cases_per_day_1.dat > single_1_data_number_of_cases_per_day_2.dat
# awk -F : '!x[$1]++ {print $1 ":" $2 ":" $3 ":"$4 ":" $5}' single_1_data_number_of_cases_per_day_2.dat > single_1_data_number_of_cases_per_day_3.dat

# Rscript ./single_data_number_of_cases_per_day.R

# # ------------------------------------------------------ Numero de Diagnosticos por dia

# awk -F : 'NR==1 {print $1":"$11":"$12":"$18} $12!="None" {print $1":"$11":"$12":"$18}' ./../Data/run_1_simulation_report.dat > single_1_data_number_of_diagnosed_1.dat
# awk -F : 'NR==1 {print $1":"$2":"$3":"$4} $2=="True" {print $1":"$2":"$3":"$4}' single_1_data_number_of_diagnosed_1.dat > single_1_data_number_of_diagnosed_2.dat
# awk -F : '!x[$1]++ {print $1":"$4}' single_1_data_number_of_diagnosed_2.dat > single_1_data_number_of_diagnosed_3.dat

# Rscript ./single_data_number_of_diagnoses_per_day.R

# # ------------------------------------------------------ Numero Total de infectados

# awk -F : '$12!="None" {print $1":"$11":"$12":"$18}' ./../Data/run_1_simulation_report.dat > single_1_data_number_of_infected_1.dat
# awk -F : '!x[$1]++ {print $1":"$2":"$3":"$4}' single_1_data_number_of_infected_1.dat > single_1_data_number_of_infected_2.dat

# # ----------------------------------------------------- Demographic Distributions

# awk -F : '!x[$1]++ {print $1":"$2":"$5}' ./../Data/run_1_simulation_report.dat > single_1_data_demographic_distributions.dat
# sed -i '1d' single_1_data_demographic_distributions.dat 

# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Populacao total

# echo "Total Population:"
# cat single_1_data_demographic_distributions.dat | wc -l
# echo "Number of Diagnosed Cases:"
# cat single_1_data_number_of_diagnosed_3.dat | wc -l
# echo "Number of infected:"
# cat single_1_data_number_of_infected_2.dat | wc -l

# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
#################################################### Code for full simulation report

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Number of Females

# awk -F : '$2=="1" {print $1":"$2}' single_1_data_demographic_distributions.dat > single_1_data_number_of_females.dat 
# echo "Number of Females:"
# cat single_1_data_number_of_females.dat | wc -l

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Age-Group Distribution

# awk -F : '$2=="1" {print $1":"$2":"$3}' single_1_data_demographic_distributions.dat > single_1_data_age_group_distribution_female.dat
# awk -F : '$2!="1" {print $1":"$2":"$3}' single_1_data_demographic_distributions.dat > single_1_data_age_group_distribution_male.dat

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Marital Status Distribution

# rm single_1_data_demographic_distributions.dat

# use R 

# rm single_1_data_number_of_infected_1.dat
# rm single_1_data_number_of_infected_2.dat

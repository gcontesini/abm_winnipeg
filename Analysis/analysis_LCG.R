library( Hmisc )

number_of_runs = 100
pop_size = 1e4

expected_household_sizes = c( 23494, 33904, 16282, 24647, 1673 )
expected_household_sizes = (expected_household_sizes / 1e5)

results = matrix( nrow = 5, ncol = number_of_runs )
summary = matrix( nrow = 2, ncol = 5 )
results[] = 0

for( run in 1:number_of_runs ) {

  inData = read.table(
    paste(
      'run_',
      run,
      '_household_size.dat',
      sep = ''
    )
  )

  h = hist(
    inData$V1,
    breaks = seq( 0, 8, by = 1),
    plot = FALSE
  )

  counts = h$counts / dim(inData[])[1]

  results[1,run] = counts[1]
  results[2,run] = counts[2]
  results[3,run] = counts[3]
  results[4,run] = sum( counts[4:5] )
  results[5,run] = sum( counts[6:8] )
}

for( i in 1:5 ) {
  summary[1,i] = mean( results[i,] )
  summary[2,i] = sd( results[i,] )
}

bp = barplot(
  summary[1,],
  ylim = c( 0, 1.2 * max( summary[1,] ) )
)

errbar(
  bp,
  summary[1,],
  ( summary[1,] + summary[2,] ),
  ( summary[1,] - summary[2,] ),
  add = TRUE
)

segments(
  bp - 0.3,
  expected_household_sizes,
  bp + 0.3,
  expected_household_sizes,
  col = '#ff0000',
  lwd = 2
)

axis(
  1,
  at = bp,
  labels = c( '1', '2', '3', '4:5', '6:8' )
)
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class setAgeGroupProbabilitiesPerGender( object ):

	# 	-h:
	#		The age probabilities for both male and female are store
	#		inside a list inside a list [[],[]] where the default is 0
	#		for male and 1 for female.
	#		gender_int = 0 return male probabilities
	#		gender_int = 1 return female probabilities

	def setAgeGroupProbabilitiesPerGender(
		self,
		gender_int_,
		age_group_probabilitie_int_
	):
		assert type( age_group_probabilitie_int_ ) == int
		assert age_group_probabilitie_int_ >= 0
		self.age_group_probabilities_per_gender_list[ gender_int_ ].append(
			age_group_probabilitie_int_
		)

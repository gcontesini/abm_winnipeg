#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import xml.etree.ElementTree as ET

class readFile( object ):

  def readFile(
    self,
    file_name_str_
  ):

    parser = ET.XMLParser(encoding="utf-8")
    tree = ET.parse( file_name_str_, parser=parser )
    root = tree.getroot()

    # ---------------------------------------------------------- Simulation Info

    self.setSimulationDuration(
      int(
        root.
        find("duration").
        text
      )
    )

    self.setPopulationSize(
      int(
        root.
        find("population_size").
        text
      )
    )

    # ---------------------------------------------------------------- Grid Info

    self.setGridSizeX(
      int(
        root.
        find("grid_size").
        find("size_x").
        text
      )
    )

    self.setGridSizeY(
      int(
        root.
        find("grid_size").
        find("size_y").
        text
      )
    )

    # ------------------------------------------------------- Gender Probability

    self.setProbabilityOfBeingMale(
      int(
        root.
        find("probability").
        find("being_male").
        text
      )
    )

    # ---------------------------------------------------- Ethnicity Probability

    self.setProbabilityOfBeingAboriginal(
      int(
        root.
        find("probability").
        find("being_aboriginal").
        text
      )
    )

    # --------------------------------------------- Marital Status Probabilities

    self.setProbabilityOfBeingSingle(
      int(
        root.
        find("cumulative_probability").
        find("marital_status").
        find("single").
        text
      )
    )

    self.setProbabilityOfBeingMarried(
      int(
        root.
        find("cumulative_probability").
        find("marital_status").
        find("married").
        text
      )
    )

    self.setProbabilityOfBeingSeparated(
      int(
        root.
        find("cumulative_probability").
        find("marital_status").
        find("separated").
        text
      )
    )

    self.setProbabilityOfBeingWidowed(
      int(
        root.
        find("cumulative_probability").
        find("marital_status").
        find("widowed").
        text
      )
    )

    # --------------------------------------------- Household Size Probabilities

    self.setProbabilityOfHouseholdSize(
      int(
        root.
        find("cumulative_probability").
        find("household").
        find("size").
        find("one_person").
        text
      )
    )

    self.setProbabilityOfHouseholdSize(
      int(
        root.
        find("cumulative_probability").
        find("household").
        find("size").
        find("two_persons").
        text
      )
    )

    self.setProbabilityOfHouseholdSize(
      int(
        root.
        find("cumulative_probability").
        find("household").
        find("size").
        find("three_persons").
        text
      )
    )

    self.setProbabilityOfHouseholdSize(
      int( root.
        find("cumulative_probability").
        find("household").
        find("size").
        find("four_five_persons").
        text
      )
    )

    self.setProbabilityOfHouseholdSize(
      int( root.
        find("cumulative_probability").
        find("household").
        find("size").
        find("six_plus_persons").
        text
      )
    )
    # ------------------------------------------------------- Disease Parameters

    self.setDiseaseInitialCases(
      int( root.
        find("initial_cases").
        text
      )
    )
    self.setExposedMinDuration(
      int( root.
        find("disease").
        find("exposed").
        find("min_duration").
        text
      )
    )
    self.setExposedMaxDuration(
      int( root.
        find("disease").
        find("exposed").
        find("max_duration").
        text
      )
    )
    self.setExposedMean(
      float( root.
        find("disease").
        find("exposed").
        find("mean").
        text
      )
    )
    self.setExposedSd(
      float( root.
        find("disease").
        find("exposed").
        find("standart_deviation").
        text
      )
    )

    self.setInfectedMinDuration(
      int( root.
        find("disease").
        find("infected").
        find("min_duration").
        text
      )
    )
    self.setInfectedMaxDuration(
      int( root.
        find("disease").
        find("infected").
        find("max_duration").
        text
      )
    )
    self.setInfectedMean(
      float( root.
        find("disease").
        find("infected").
        find("mean").
        text
      )
    )
    self.setInfectedSd(
      float( root.
        find("disease").
        find("infected").
        find("standart_deviation").
        text
      )
    )

    # -------------------------------------------------- Age Group Probabilities

    age_text = \
      str( root.
        find("age_gender_profile").
        text
      )

    for lines_str in age_text.splitlines():

      word_list = []
      word_list = lines_str.split()

      if len(word_list) != 0:

        self.setAgeGroupProbabilitiesPerGender(
          0,
          int( word_list[ 1 ] )
        )
        self.setAgeGroupProbabilitiesPerGender(
          1,
          int( word_list[ 2 ] )
        )

    # ------------------------------------------------------------ Group Related

    # --------------------------------------------------------------------- Work

    self.setWorkGroupMean(
      float( root.
        find("group").
        find("statistics").
        find("work_group").
        find("mean").
        text
      )
    )

    self.setWorkGroupSD(
      float( root.
        find("group").
        find("statistics").
        find("work_group").
        find("standart_deviation").
        text
      )
    )

    self.setWorkGroupSize(
      float( root.
        find("group").
        find("statistics").
        find("work_group").
        find("size").
        text
      )
    )

    self.setWorkGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("work_group").
        find("initial").
        text
      )
    )

    self.setWorkGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("work_group").
        find("final").
        text
      )
    )

    # ----------------------------------------------------------------- Day-Care

    self.setDayCareGroupMean(
      float( root.
        find("group").
        find("statistics").
        find("day_care_group").
        find("mean").
        text
      )
    )

    self.setDayCareGroupSD(
      float( root.
        find("group").
        find("statistics").
        find("day_care_group").
        find("standart_deviation").
        text
      )
    )

    self.setDayCareGroupSize(
      float( root.
        find("group").
        find("statistics").
        find("day_care_group").
        find("size").
        text
      )
    )

    self.setDayCareGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("day_care_group").
        find("initial").
        text
      )
    )

    self.setDayCareGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("day_care_group").
        find("final").
        text
      )
    )

    # ------------------------------------------------------------------- School

    self.setSchoolGroupMean(
      float( root.
        find("group").
        find("statistics").
        find("school_group").
        find("mean").
        text
      )
    )

    self.setSchoolGroupSD(
      float( root.
        find("group").
        find("statistics").
        find("school_group").
        find("standart_deviation").
        text
      )
    )

    self.setSchoolGroupSize(
      float( root.
        find("group").
        find("statistics").
        find("school_group").
        find("size").
        text
      )
    )

    self.setSchoolGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("school_group").
        find("initial").
        text
      )
    )

    self.setSchoolGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("school_group").
        find("final").
        text
      )
    )

    # ------------------------------------------------------------------- Social

    self.setSocialGroupMean(
      float( root.
        find("group").
        find("statistics").
        find("social_group").
        find("mean").
        text
      )
    )

    self.setSocialGroupSD(
      float( root.
        find("group").
        find("statistics").
        find("social_group").
        find("standart_deviation").
        text
      )
    )

    self.setSocialGroupSize(
      float( root.
        find("group").
        find("statistics").
        find("social_group").
        find("size").
        text
      )
    )

    self.setSocialGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("social_group").
        find("initial").
        text
      )
    )

    self.setSocialGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("social_group").
        find("final").
        text
      )
    )

    # ---------------------------------------------------------- LeisureActivity

    self.setLeisureActivityGroupMean(
      float( root.
        find("group").
        find("statistics").
        find("leisure_activity_group").
        find("mean").
        text
      )
    )

    self.setLeisureActivityGroupSD(
      float( root.
        find("group").
        find("statistics").
        find("leisure_activity_group").
        find("standart_deviation").
        text
      )
    )

    self.setLeisureActivityGroupSize(
      float( root.
        find("group").
        find("statistics").
        find("leisure_activity_group").
        find("size").
        text
      )
    )

    self.setLeisureActivityGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("leisure_activity_group").
        find("initial").
        text
      )
    )

    self.setLeisureActivityGroupAgeGroup(
      float( root.
        find("group").
        find("age").
        find("leisure_activity_group").
        find("final").
        text
      )
    )

    # ------------------------------------------------------ Age Difference Info


    self.setAgentPartnerAgeDifferenceMean(
      float( root.
        find("age_difference").
        find("agent_partner").
        find("mean").
        text
      )
    )


    self.setAgentPartnerAgeDifferenceSD(
      float( root.
        find("age_difference").
        find("agent_partner").
        find("standart_deviation").
        text
      )
    )

      # ----------------------------------------------------------- Mother Child

    self.setMotherChildAgeDifferenceMean(
      float( root.
        find("age_difference").
        find("mother_child").
        find("mean").
        text
      )
    )

    self.setMotherChildAgeDifferenceSD(
      float( root.
        find("age_difference").
        find("mother_child").
        find("standart_deviation").
        text
      )
    )
    
    # ------------------------------------------------------- Disease Diagnostic

    self.setProbabilityOfDiseaseDiagnostic(
      int( root.
        find("probability_of_diagnostic").
        text
      )
    )
    # ------------------------------------------------------- Probability of Stay Home Sick

    self.setProbabilityOfStayHomeSick(
      int( root.
        find("probability_stay_home_sick").
        text
      )
    )
    # ------------------------------------------ Disease Infection Probabilities

    self.setProbabilityOfInfection(
      int( root.
        find("infection_probabilities").
        text
      )
    )
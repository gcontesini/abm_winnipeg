#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from simulation_parameters_methods import*

class SimulationParameters(
  getAgeGroupProbabilitiesPerGender                 ,
  getAgentPartnerAgeDifferenceMean                  ,
  getAgentPartnerAgeDifferenceSD                    ,
  getDayCareGroupAgeGroup                           ,
  getDayCareGroupMean                               ,
  getDayCareGroupSD                                 ,
  getDayCareGroupSize                               ,
  getDiseaseInitialCases                            ,
  getExposedMaxDuration                             ,
  getExposedMean                                    ,
  getExposedMinDuration                             ,
  getExposedSd                                      ,
  getGridSizeX                                      ,
  getGridSizeY                                      ,
  getInfectedMaxDuration                            ,
  getInfectedMean                                   ,
  getInfectedMinDuration                            ,
  getInfectedSd                                     ,
  getLeisureActivityGroupAgeGroup                   ,
  getLeisureActivityGroupMean                       ,
  getLeisureActivityGroupSD                         ,
  getLeisureActivityGroupSize                       ,
  getMotherChildAgeDifferenceMean                   ,
  getMotherChildAgeDifferenceSD                     ,
  getPercentageOfDayCareInteractions                ,
  getPercentageOfHomeInteractions                   ,
  getPercentageOfLeisureActivityInteractions        ,
  getPercentageOfRandomInteractions                 ,
  getPercentageOfSchoolInteractions                 ,
  getPercentageOfSocialInteractions                 ,
  getPercentageOfWorkInteractions                   ,
  getPopulationSize                                 ,
  getProbabilityOfDiseaseDiagnostic                 ,
  getProbabilityOfInfection                         ,
  getProbabilityOfInfectionAtDayCare                ,
  getProbabilityOfInfectionAtHome                   ,
  getProbabilityOfInfectionAtLeisureActivity        ,
  getProbabilityOfInfectionAtRandom                 ,
  getProbabilityOfInfectionAtSchool                 ,
  getProbabilityOfInfectionAtSocial                 ,
  getProbabilityOfInfectionAtWork                   ,
  getProbabilityOfBeingAboriginal                   ,
  getProbabilityOfBeingMale                         ,
  getProbabilityOfBeingMarried                      ,
  getProbabilityOfBeingSeparated                    ,
  getProbabilityOfBeingSingle                       ,
  getProbabilityOfBeingWidowed                      ,
  getProbabilityOfHouseholdSize                     ,
  getSchoolGroupAgeGroup                            ,
  getSchoolGroupMean                                ,
  getSchoolGroupSD                                  ,
  getSchoolGroupSize                                ,
  getSimulationDuration                             ,
  getSocialGroupAgeGroup                            ,
  getSocialGroupMean                                ,
  getSocialGroupSD                                  ,
  getSocialGroupSize                                ,
  getWorkGroupAgeGroup                              ,
  getWorkGroupMean                                  ,
  getWorkGroupSD                                    ,
  getWorkGroupSize                                  ,
  readFile                                          ,
  setAgeGroupProbabilitiesPerGender                 ,
  setAgentPartnerAgeDifferenceMean                  ,
  setAgentPartnerAgeDifferenceSD                    ,
  setDayCareGroupAgeGroup                           ,
  setDayCareGroupMean                               ,
  setDayCareGroupSD                                 ,
  setDayCareGroupSize                               ,
  setDiseaseInitialCases                            ,
  setExposedMaxDuration                             ,
  setExposedMean                                    ,
  setExposedMinDuration                             ,
  setExposedSd                                      ,
  setGridSizeX                                      ,
  setGridSizeY                                      ,
  setInfectedMaxDuration                            ,
  setInfectedMean                                   ,
  setInfectedMinDuration                            ,
  setInfectedSd                                     ,
  setLeisureActivityGroupAgeGroup                   ,
  setLeisureActivityGroupMean                       ,
  setLeisureActivityGroupSD                         ,
  setLeisureActivityGroupSize                       ,
  setMotherChildAgeDifferenceMean                   ,
  setMotherChildAgeDifferenceSD                     ,
  setPercentageOfDayCareInteractions                ,
  setPercentageOfHomeInteractions                   ,
  setPercentageOfLeisureActivityInteractions        ,
  setPercentageOfRandomInteractions                 ,
  setPercentageOfSchoolInteractions                 ,
  setPercentageOfSocialInteractions                 ,
  setPercentageOfWorkInteractions                   ,
  setPopulationSize                                 ,
  setProbabilityOfDiseaseDiagnostic                 ,
  setProbabilityOfStayHomeSick                      ,
  getProbabilityOfStayHomeSick                      ,
  setProbabilityOfInfection                         ,
  setProbabilityOfInfectionAtDayCare                ,
  setProbabilityOfInfectionAtHome                   ,
  setProbabilityOfInfectionAtLeisureActivity        ,
  setProbabilityOfInfectionAtRandom                 ,
  setProbabilityOfInfectionAtSchool                 ,
  setProbabilityOfInfectionAtSocial                 ,
  setProbabilityOfInfectionAtWork                   ,
  setProbabilityOfBeingAboriginal                   ,
  setProbabilityOfBeingMale                         ,
  setProbabilityOfBeingMarried                      ,
  setProbabilityOfBeingSeparated                    ,
  setProbabilityOfBeingSingle                       ,
  setProbabilityOfBeingWidowed                      ,
  setProbabilityOfHouseholdSize                     ,
  setSchoolGroupAgeGroup                            ,
  setSchoolGroupMean                                ,
  setSchoolGroupSD                                  ,
  setSchoolGroupSize                                ,
  setSimulationDuration                             ,
  setSocialGroupAgeGroup                            ,
  setSocialGroupMean                                ,
  setSocialGroupSD                                  ,
  setSocialGroupSize                                ,
  setWorkGroupAgeGroup                              ,
  setWorkGroupMean                                  ,
  setWorkGroupSD                                    ,
  setWorkGroupSize
):

# ------------------------------------------------------------------ Constructor

  def __init__(
    self,
    file_name_
   ):

    # -------------------------------------------------------- Static Attributes

    self.simulation_duration_int = None
    self.population_size_int = None
    self.grid_size_x_int = None
    self.grid_size_y_int = None

    # ------------------------------------------------------------ Probabilities

    self.probability_of_being_male_int = None
    self.probability_of_being_aboriginal_int = None
    self.probability_of_being_single_int = None
    self.probability_of_being_married_int = None
    self.probability_of_being_separated_int = None
    self.probability_of_being_widowed_int = None
    self.probability_of_living_alone_int = None
    self.probability_of_living_with_relatives_int = None
    self.probability_of_living_with_non_relatives_int = None

    self.probability_of_household_size_list = []
    self.age_group_probabilities_per_gender_list = [[],[]]

    # --------------------------------------------------------- Group Attributes

    self.work_group_mean_float = None
    self.work_group_sd_float = None
    self.work_group_size_float = None
    self.work_group_age_group_tpl = []

    self.day_care_group_mean_float = None
    self.day_care_group_sd_float = None
    self.day_care_group_size_float = None
    self.day_care_group_age_group_tpl = []

    self.school_group_mean_float = None
    self.school_group_sd_float = None
    self.school_group_size_float = None
    self.school_group_age_group_tpl = []

    self.social_group_mean_float = None
    self.social_group_sd_float = None
    self.social_group_size_float = None
    self.social_group_age_group_tpl = []

    self.leisure_activity_group_mean_float = None
    self.leisure_activity_group_sd_float = None
    self.leisure_activity_gorup_size_float = None
    self.leisure_activity_group_age_group_tpl = []

    # ------------------------------------------------------------------ Disease

    self.disease_initial_cases_int = None

    self.exposed_min_duration_int = None
    self.exposed_max_duration_int = None
    self.exposed_mean_float = None
    self.exposed_sd_float = None

    self.infected_min_duration_int = None
    self.infected_max_duration_int = None
    self.infected_mean_float = None
    self.infected_sd_float = None

    # ----------------------------------------------------------- Age Difference

    self.agent_partner_age_difference_mean_float = None
    self.agent_partner_age_difference_sd_float = None

    self.mother_child_age_difference_mean_float = None
    self.mother_child_age_difference_sd_float = None

    # --------------------------------------------------- Number of Interactions

    self.percentage_of_home_interactions_int = None
    self.percentage_of_work_interactions_int = None
    self.percentage_of_school_interactions_int = None
    self.percentage_of_day_care_interactions_int = None
    self.percentage_of_social_interactions_int = None
    self.percentage_of_leisure_activity_interactions_int = None
    self.percentage_of_random_interactions_int = None

    # ---------------------------------------------- Probabilities of Diagnostic
    
    self.probability_of_disease_diagonstic_int = None

    # ------------------------------------------------ Probabilities of Stay Home Sick

    self.probability_of_stay_home_sick = None

    # ------------------------------------------------ Probabilities Trasnmition

    self.probability_of_infection_int = None

    self.probability_of_infection_at_home_int = None
    self.probability_of_infection_at_work_int = None
    self.probability_of_infection_at_random_int = None
    self.probability_of_infection_at_social_int = None
    self.probability_of_infection_at_school_int = None
    self.probability_of_infection_at_day_care_int = None
    self.probability_of_infection_at_leisure_activity_int = None

    # ------------------------------------------------------------- Reading File

    self.readFile( file_name_ )

# -------------------------------------------------------------------------- END

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from demographic_counters_methods import  *

class DemographicCounters(

calcMaxNumOfAdultsAllowed,
calcMaxNumOfAgentsPerAgeGroup,
calcMaxNumOfChildrenAllowed,
calcMaxNumOfHouseholdSizes,
decreaseNumOfAdultsAllowed,
decreaseNumOfAgentsForAgeGroup,
decreaseNumOfChildrenAllowed,
decreaseNumOfHouseholdSizeAllowed,
decreaseTotalNumOfAboriginal,
decreaseTotalNumOfAdults,
decreaseTotalNumOfAgents,
decreaseTotalNumOfChildren,
decreaseTotalNumOfDayCareGroups,
decreaseTotalNumOfFamilyGroups,
decreaseTotalNumOfFemales,
decreaseTotalNumOfHouseholdSize,
decreaseTotalNumOfKids,
decreaseTotalNumOfLeisureActivityGroups,
decreaseTotalNumOfMales,
decreaseTotalNumOfMarried,
decreaseTotalNumOfSchoolGroups,
decreaseTotalNumOfSeparated,
decreaseTotalNumOfSingle,
decreaseTotalNumOfSocialGroups,
decreaseTotalNumOfWidowed,
decreaseTotalNumOfWorkGroups,
getMaxNumOfAdultsAllowed,
getMaxNumOfAgentsForAgeGroup,
getMaxNumOfChildrenAllowed,
getMaxNumOfHouseholdSizeAllowed,
getTotalNumOfAboriginal,
getTotalNumOfAdults,
getTotalNumOfAgents,
getTotalNumOfChildren,
getTotalNumOfDayCareGroups,
getTotalNumOfFamilyGroups,
getTotalNumOfFemales,
getTotalNumOfHouseholdSize,
getTotalNumOfKids,
getTotalNumOfLeisureActivityGroups,
getTotalNumOfMales,
getTotalNumOfMarried,
getTotalNumOfSchoolGroups,
getTotalNumOfSeparated,
getTotalNumOfSingle,
getTotalNumOfSocialGroups,
getTotalNumOfWidowed,
getTotalNumOfWorkGroups,
increaseNumOfHouseholdSizeAllowed,
increaseTotalNumOfAboriginal,
increaseTotalNumOfAgents,
increaseTotalNumOfChildren,
increaseTotalNumOfDayCareGroups,
increaseTotalNumOfFamilyGroups,
increaseTotalNumOfFemales,
increaseTotalNumOfHouseholdSize,
increaseTotalNumOfKids,
increaseTotalNumOfLeisureActivityGroups,
increaseTotalNumOfMales,
increaseTotalNumOfMarried,
increaseTotalNumOfSchoolGroups,
increaseTotalNumOfSeparated,
increaseTotalNumOfSingle,
increaseTotalNumOfSocialGroups,
increaseTotalNumOfWidowed,
increaseTotalNumOfWorkGroups       

):

#--------------------------------------------------------------------Constructor

  def __init__(
    self,
    simulation_parameters_obj_
   ):

#---------------------------------------------------------------------Parameters

    #~~# number of agents #~~#

    self.total_num_of_agents_int = 1

    #~~# group #~~#

    self.total_num_of_family_groups_int = 0
    self.total_num_of_work_groups_int = 0
    self.total_num_of_day_care_groups_int = 0
    self.total_num_of_school_groups_int = 0
    self.total_num_of_social_groups_int = 0
    self.total_num_of_leisure_activity_groups_int = 0

    #~~# gender #~~#

    self.total_num_of_males_int = 0
    self.total_num_of_females_int = 0

    self.total_num_of_adults_int = 0
    self.total_num_of_children_int = 0
    
    #~~# household #~~#

    self.num_of_household_sizes = np.zeros(5,dtype=int)

    #~~# agent characteristics #~~#

    self.total_num_of_aboriginal_int = 0

    self.total_num_of_single_int = 0
    self.total_num_of_married_int = 0
    self.total_num_of_separated_int = 0
    self.total_num_of_widowed_int = 0
    self.total_num_of_kids_int = 0
  
    self.max_num_agents_per_age_group_list = [ [], [] ]
    self.max_num_of_household_sizes_list = np.zeros(5,dtype=int)
    self.max_num_adults_int = 0
    self.max_num_children_int = 0

    #~~# initial conditions #~~#

    self.simulation_parameters_obj = simulation_parameters_obj_

    #--------------------------------------------------------------------Setters

    self.calcMaxNumOfAgentsPerAgeGroup()
    self.calcMaxNumOfAdultsAllowed()
    self.calcMaxNumOfChildrenAllowed()
    self.calcMaxNumOfHouseholdSizes()

#----------------------------------------------------------------------------End

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import numpy as np

from calc_max_num_of_adults_allowed					import	*
from calc_max_num_of_agents_per_age_group			import	*
from calc_max_num_of_children_allowed				import	*
from calc_max_num_of_household_sizes				import 	*
from decrease_num_of_adults_allowed					import	*
from decrease_num_of_agents_for_age_group			import	*
from decrease_num_of_children_allowed				import	*
from decrease_num_of_household_size_allowed			import	*
from decrease_total_num_of_aboriginal				import	*
from decrease_total_num_of_adults					import	*
from decrease_total_num_of_agents					import	*
from decrease_total_num_of_children					import	*
from decrease_total_num_of_day_care_groups			import	*
from decrease_total_num_of_family_groups			import	*
from decrease_total_num_of_females					import	*
from decrease_total_num_of_household_size			import 	*
from decrease_total_num_of_infected_agents			import	*
from decrease_total_num_of_kids						import	*
from decrease_total_num_of_leisure_activity_groups	import	*
from decrease_total_num_of_males					import	*
from decrease_total_num_of_married					import	*
from decrease_total_num_of_school_groups			import	*
from decrease_total_num_of_separated				import	*
from decrease_total_num_of_single					import	*
from decrease_total_num_of_social_groups			import	*
from decrease_total_num_of_widowed					import	*
from decrease_total_num_of_work_groups				import	*
from get_max_num_of_adults_allowed					import	*
from get_max_num_of_agents_for_age_group			import	*
from get_max_num_of_children_allowed				import	*
from get_max_num_of_household_size_allowed			import 	*
from get_total_num_of_aboriginal					import	*
from get_total_num_of_adults						import	*
from get_total_num_of_agents						import	*
from get_total_num_of_children						import	*
from get_total_num_of_day_care_groups				import	*
from get_total_num_of_family_groups					import	*
from get_total_num_of_females						import	*
from get_total_num_of_household_size				import 	*
from get_total_num_of_kids							import	*
from get_total_num_of_leisure_activity_groups		import	*
from get_total_num_of_males 						import	*
from get_total_num_of_married						import	*
from get_total_num_of_school_groups					import	*
from get_total_num_of_separated						import	*
from get_total_num_of_single						import	*
from get_total_num_of_social_groups					import	*
from get_total_num_of_widowed						import	*
from get_total_num_of_work_groups					import	*
from increase_num_of_household_size_allowed			import	*
from increase_total_num_of_aboriginal				import	*
from increase_total_num_of_agents					import	*
from increase_total_num_of_children					import	*
from increase_total_num_of_day_care_groups			import	*
from increase_total_num_of_family_groups			import	*
from increase_total_num_of_females					import	*
from increase_total_num_of_household_size 			import 	*
from increase_total_num_of_infected_agents			import	*
from increase_total_num_of_kids						import	*
from increase_total_num_of_leisure_activity_groups	import	*
from increase_total_num_of_males 					import	*
from increase_total_num_of_married					import	*
from increase_total_num_of_school_groups			import	*
from increase_total_num_of_separated				import	*
from increase_total_num_of_single					import	*
from increase_total_num_of_social_groups			import	*
from increase_total_num_of_widowed					import	*
from increase_total_num_of_work_groups				import	*
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class calcMaxNumOfHouseholdSizes( object ):

  def calcMaxNumOfHouseholdSizes( self ):
  
    increase_float = 1.10

    self.max_num_of_household_sizes_list[ 0 ] = \
    increase_float * \
    self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[0] *\
    self.simulation_parameters_obj.getPopulationSize() / ( 1E5)

    for i_int in range(1,len(self.max_num_of_household_sizes_list )):
      
      self.max_num_of_household_sizes_list[ i_int ] = \
        increase_float *\
        (
          self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[ i_int ] -\
          self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[ i_int-1 ] 
        ) *\
        self.simulation_parameters_obj.getPopulationSize() / ( 1E5)

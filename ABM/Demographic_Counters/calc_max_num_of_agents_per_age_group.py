#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class calcMaxNumOfAgentsPerAgeGroup( object ):

  def calcMaxNumOfAgentsPerAgeGroup( self ):
    
    male_int = 0
    female_int = 1

    child_increase_float = 1.15
    adult_increase_float = 1.10
    elder_increase_float = 1.30

    male_age_list = \
      self.simulation_parameters_obj.getAgeGroupProbabilitiesPerGender( male_int )
    female_age_list = \
      self.simulation_parameters_obj.getAgeGroupProbabilitiesPerGender( female_int )

    self.max_num_agents_per_age_group_list[ male_int ].append(
      int(
          male_age_list[ 0 ] * child_increase_float *
          self.simulation_parameters_obj.getProbabilityOfBeingMale() *
          self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2
      )
    )

    self.max_num_agents_per_age_group_list[ female_int ].append(
      int(
          female_age_list[ 0 ] * child_increase_float *
          (100000 - self.simulation_parameters_obj.getProbabilityOfBeingMale() ) *
          self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2 
      )
    )

    for i_int in range( 1, len( male_age_list ) ):

      if( i_int <= 4):

        self.max_num_agents_per_age_group_list[ male_int ].append(
          int(
              ( male_age_list[ i_int ] - male_age_list[ i_int - 1 ] ) * child_increase_float *
              self.simulation_parameters_obj.getProbabilityOfBeingMale() *            
              self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2 
          )
        )

        self.max_num_agents_per_age_group_list[ female_int ].append(
          int(
              ( female_age_list[ i_int ] - female_age_list[ i_int - 1 ] ) * child_increase_float *
              ( 100000 - self.simulation_parameters_obj.getProbabilityOfBeingMale() ) *
              self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2 
          )
        )

      elif( i_int <= 12 ):

        self.max_num_agents_per_age_group_list[ male_int ].append(
          int(
              ( male_age_list[ i_int ] - male_age_list[ i_int - 1 ] ) * adult_increase_float *
              self.simulation_parameters_obj.getProbabilityOfBeingMale() *            
              self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2 
          )
        )

        self.max_num_agents_per_age_group_list[ female_int ].append(
          int(
              ( female_age_list[ i_int ] - female_age_list[ i_int - 1 ] ) * adult_increase_float *
              ( 100000 - self.simulation_parameters_obj.getProbabilityOfBeingMale() ) *
              self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2 
          )
        )

      else:

        self.max_num_agents_per_age_group_list[ male_int ].append(
          int(
              ( male_age_list[ i_int ] - male_age_list[ i_int - 1 ] ) * elder_increase_float *
              self.simulation_parameters_obj.getProbabilityOfBeingMale() *            
              self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2 
          )
        )

        self.max_num_agents_per_age_group_list[ female_int ].append(
          int(
              ( female_age_list[ i_int ] - female_age_list[ i_int - 1 ] ) * elder_increase_float *
              ( 100000 - self.simulation_parameters_obj.getProbabilityOfBeingMale() ) *
              self.simulation_parameters_obj.getPopulationSize() / ( 1E5 )**2 
          )
        )

  # Importante Note:
  #   The sum of the maximum number of agents
  #   per age group from the male and female
  #   groups should be the equal to the population size	
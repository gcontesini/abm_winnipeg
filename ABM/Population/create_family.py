#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Groups.groups as groups_class

class createFamily( object ):

  def createFamily(
    self,
    agent_obj
  ):

    family_group_obj = groups_class.Groups( self.counters_obj.getTotalNumOfFamilyGroups() )

    family_group_obj.setGroupAsFamily()

    self.counters_obj.increaseTotalNumOfFamilyGroups()

    grid_pos_tpl = self.grid_obj.chooseRandPosition( )

    family_group_obj.setGroupPositionX( grid_pos_tpl[0] )
    family_group_obj.setGroupPositionY( grid_pos_tpl[1] )

    # ---------------------------------------------- Family

    agent_obj.addFamilyGroupToAgent( family_group_obj )

    family_group_obj.addAgentToGroup( agent_obj )
    
    # ---------------------------------------------- Family Size

    household_size_int = self.chooseHouseholdSize( agent_obj )

    # ---------------------------------------------- Family Composition

    agent_obj.setAgentCurrentPositionX( agent_obj.getAgentFamilyGroup().getGroupPositionX() )
    agent_obj.setAgentCurrentPositionY( agent_obj.getAgentFamilyGroup().getGroupPositionY() )

    self.chooseNextPosition( agent_obj )

    self.grid_obj.addAgentToPosition(
      agent_obj.getAgentFamilyGroup().getGroupPositionX(),
      agent_obj.getAgentFamilyGroup().getGroupPositionY(),
      agent_obj
    )
    # ---------------------------------------------- Agent Marital Status

    self.chooseMaritalStatus( agent_obj, household_size_int )

    # ---------------------------------------------- Agent Family Agents

    self.createHouseholdStructure( agent_obj, household_size_int )
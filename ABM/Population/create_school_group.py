#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Groups.groups as groups_class

class createSchoolGroup( object ):

  def createSchoolGroup( self ):

    children_list = self.getAgentAgeGroupFromGrid(
      self.simulation_parameters_obj.getSchoolGroupAgeGroup()[0],
      self.simulation_parameters_obj.getSchoolGroupAgeGroup()[1]
    )
    children_list = self.sortAgentList( children_list )
    
    children_index_int = 0

    while( children_index_int < len( children_list ) ):

      if( children_list[ children_index_int ].getAgentWorkGroup() == None ):

        work_group_obj = groups_class.Groups(
          self.counters_obj.getTotalNumOfWorkGroups()
        ) 

        self.counters_obj.increaseTotalNumOfSchoolGroups()

        work_group_obj.setGroupAsSchool()
        
        # --------------------------------------------------------- Group Position
   
        work_position_tuple = self.grid_obj.chooseRandPosition()

        work_group_obj.setGroupPositionX( work_position_tuple[0] )
        work_group_obj.setGroupPositionY( work_position_tuple[1] )

        # ------------------------------------------------------- Group Properties

        group_size_int = \
          self.simulation_parameters_obj.getSchoolGroupSize() + \
          int(
            round(
              self.normalDistribution(
                self.simulation_parameters_obj.getSchoolGroupMean(),
                self.simulation_parameters_obj.getSchoolGroupSD()
              )
            )
          )

        if( group_size_int > ( len( children_list ) - children_index_int + 1 ) ):

          group_size_int = ( len( children_list ) - children_index_int + 1 )

        # ---------------------------------------------------------- Group Teacher

        self.chooseTeacherToTheGroup( work_group_obj )

        # ------------------------------------------------------ Add First Student

        children_list[ children_index_int ].\
          addWorkGroupToAgent( work_group_obj )

        work_group_obj.\
          addAgentToGroup(
            children_list[ children_index_int ]
          )

        # --------------------------------------------------- Add Rest of Students

        
        while( work_group_obj.getNumberOfMembersInTheGroup() < group_size_int ):
          
          children_index_int = children_index_int + 1

          if( children_list[ children_index_int ].getAgentWorkGroup() == None ):

            children_list[ children_index_int ].\
              addWorkGroupToAgent( work_group_obj )

            work_group_obj.\
              addAgentToGroup( children_list[ children_index_int ] )

      children_index_int = children_index_int + 1
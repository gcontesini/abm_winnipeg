#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from population_methods import*

class Population(

chooseAdultAge,
chooseChildAge,
chooseEthnicity,
chooseGender,
chooseHouseholdSize,
chooseListRandomIndex,
chooseMaritalStatus,
chooseNextPosition,
choosePartnerAge,
choosePartnerGender,
chooseTeacherToTheGroup,
convertAgeToIndex,
convertIndexToAge,
createAdults,
createAgent,
createChildren,
createLeisureActivityGroup,
createSocialGroup,
createDayCareGroup,
createFamily,
createHouseholdStructure,
createPartner,
createPopulation,
createSchoolGroup,
createWorkGroup,
getAgentAgeGroupFromGrid,
getRandomNumber,
getRandomNumberDefault,
logNormalDistribution,
moveAgentsToHome,
moveAgentsToWorkplace,
moveGroupToPosition,
normalDistribution,
sampleElementFromList,
sortAgentList             

):

  #----------------------------------------------------------------- Constructor

  '''
  Parameters:
      object  simulation_parameters_obj
      object  counters_obj
      object  grid_obj

  Attributes:
      None

  '''

  def __init__(
    self,
    simulation_parameters_obj_,
    counters_obj_,
    grid_obj_
  ):

    self.simulation_parameters_obj = simulation_parameters_obj_
    self.counters_obj = counters_obj_
    self.grid_obj = grid_obj_

    # -------------------------------------------------------- Create Population

    self.createPopulation()

# -------------------------------------------------------------------------- End
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class moveAgentsToWorkplace( object ):

  def moveAgentsToWorkplace( self ):

    for x_int in range ( self.grid_obj.getGridSizeX() ):

      for y_int in range ( self.grid_obj.getGridSizeY() ):

        if( type( self.grid_obj.grid_mtx[ x_int ][ y_int ] ) == list ):

          list_size_int = len( self.grid_obj.grid_mtx[ x_int ][ y_int ] )

          for index_int in range( list_size_int-1,-1,-1 ):

            agent_obj = self.grid_obj.grid_mtx[ x_int ][ y_int ][ index_int ]

            self.grid_obj.moveAgentToPosition( agent_obj ) 
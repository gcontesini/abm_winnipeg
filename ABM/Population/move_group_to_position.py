#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class moveGroupToPosition( object ):

  def moveGroupToPosition(
    self,
    group_obj_
  ):

    group_position_tpl = self.chooseGridPosition()

    for index_int in range( group_obj_.getNumberOfMembersInTheGroup() ):

      agent_obj = group_obj_.getAgent( index_int )

      agent_obj.setAgentNextPositionX(group_position_tpl[0])

      agent_obj.setAgentNextPositionY(group_position_tpl[1])

      self.grid_obj.moveAgentToPosition( agent_obj )

    group_obj_.setGroupPositionX( group_position_tpl[0] )
    group_obj_.setGroupPositionY( group_position_tpl[1] )



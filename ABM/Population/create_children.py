#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Agents.agents as agents_class
import random as random

class createChildren( object ):

  def createChildren(
    self,
    agent_obj_,
    household_size_
  ):

    male_int = 0
    female_int = 1

    num_of_children = (
      household_size_
      -
      agent_obj_.getAgentFamilyGroup().getNumberOfMembersInTheGroup()
    )

    while( 
      num_of_children > 0
      and
      self.counters_obj.getTotalNumOfAgents()
      <
      self.simulation_parameters_obj.getPopulationSize()
    ):
    
      gender_int = self.chooseGender( )

      age_int = self.chooseChildAge(
        agent_obj_,
        gender_int
      )

      if( type( age_int ) == int ):

        # --------------------------------------------------- Child Object

        child_obj = agents_class.Agents(
          self.counters_obj.getTotalNumOfAgents()
        )

        # --------------------------------------------------- Counters Related

        self.counters_obj.increaseTotalNumOfAgents()

        num_of_children = num_of_children - 1

        # --------------------------------------------------------- Child Family

        agent_obj_.getAgentFamilyGroup().addAgentToGroup( child_obj )        
        
        child_obj.addFamilyGroupToAgent( agent_obj_.getAgentFamilyGroup() )      

        # --------------------------------------------------- Child Grid Related

        child_obj.setAgentCurrentPositionX( child_obj.getAgentFamilyGroup().getGroupPositionX() )
        child_obj.setAgentCurrentPositionY( child_obj.getAgentFamilyGroup().getGroupPositionY() )

        self.grid_obj.addAgentToPosition(
          child_obj.getAgentFamilyGroup().getGroupPositionX (),
          child_obj.getAgentFamilyGroup().getGroupPositionY(),
          child_obj
        )       

        # --------------------------------------------------- Child Age

        child_obj.setAgentAge( age_int )

        age_index_int = self.convertAgeToIndex( age_int )

        self.counters_obj.decreaseNumOfAgentsForAgeGroup(
          gender_int,
          age_index_int
        )

        if( age_int < 18 ):

          self.counters_obj.decreaseNumOfChildrenAllowed()

          child_obj.setAgentAsKid()

          self.counters_obj.increaseTotalNumOfKids()

          child_obj.setAgentNextPositionX( child_obj.getAgentCurrentPositionX() )
          child_obj.setAgentNextPositionY( child_obj.getAgentCurrentPositionY() )          

        if( age_int >= 18):

          self.counters_obj.decreaseNumOfAdultsAllowed()

          child_obj.setAgentAsSingle()

          self.counters_obj.increaseTotalNumOfSingle()

          self.chooseNextPosition( child_obj )

        # --------------------------------------------------- Child Gender

        if( gender_int == male_int ):

          child_obj.setAgentAsMale()
          self.counters_obj.increaseTotalNumOfMales()

        if( gender_int == female_int ):

          child_obj.setAgentAsFemale()
          self.counters_obj.increaseTotalNumOfFemales()

      if( type( age_int ) == bool ):

        num_of_children = num_of_children - 1

        household_size = household_size_ - 1 


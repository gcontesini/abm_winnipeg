#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Agents.agents as agents_class

class createPartner( object ):

  def createPartner(
    self,
    agent_obj_,
    household_size_
  ):
    # Note:
    #   male = 0
    #   female = 1

    male_int = 0
    female_int = 1

    gender_int = self.choosePartnerGender( agent_obj_ )
    
    partner_age_int = self.choosePartnerAge( agent_obj_ , gender_int )

    if( type( partner_age_int ) == int ):

      # ---------------------------------------------- Partner Object

      partner_obj = agents_class.Agents(
        self.counters_obj.getTotalNumOfAgents()
      )

        # ---------------------------------------------- Counter Related

      self.counters_obj.increaseTotalNumOfAgents()

      self.counters_obj.decreaseNumOfAdultsAllowed()

        # ---------------------------------------------- Partner Age

      partner_obj.setAgentAge( partner_age_int ) 

      age_index_int = self.convertAgeToIndex( partner_age_int )

      self.counters_obj.decreaseNumOfAgentsForAgeGroup(
        gender_int,
        age_index_int
      )

        # ---------------------------------------------- Partner Gender

      if( gender_int == male_int ):

        partner_obj.setAgentAsMale()

        self.counters_obj.increaseTotalNumOfMales()

      elif( gender_int == female_int ):

        partner_obj.setAgentAsFemale()

        self.counters_obj.increaseTotalNumOfFemales()

      # ---------------------------------------------- Partner Ethnicity

      self.chooseEthnicity( partner_obj )

      # ---------------------------------------------- Partner Marital Status

      partner_obj.setAgentAsMarried()
      self.counters_obj.increaseTotalNumOfMarried()

      # ---------------------------------------------- Partner Family

      agent_obj_.getAgentFamilyGroup().addAgentToGroup( partner_obj )        

      partner_obj.addFamilyGroupToAgent( agent_obj_.getAgentFamilyGroup() )

      # ---------------------------------------------- Partner Grid Related

      partner_obj.setAgentCurrentPositionX(
        partner_obj.getAgentFamilyGroup().getGroupPositionX()
      )
      partner_obj.setAgentCurrentPositionY(
        partner_obj.getAgentFamilyGroup().getGroupPositionY()
      )

      self.chooseNextPosition( partner_obj )

      self.grid_obj.addAgentToPosition(
        partner_obj.getAgentFamilyGroup().getGroupPositionX(),
        partner_obj.getAgentFamilyGroup().getGroupPositionY(),
        partner_obj
      )

      assert agent_obj_.getAgentCurrentPositionX() == partner_obj.getAgentCurrentPositionX()
      assert agent_obj_.getAgentCurrentPositionY() == partner_obj.getAgentCurrentPositionY()

      assert self.grid_obj.grid_mtx[ 
        partner_obj.getAgentCurrentPositionX()
        ][ partner_obj.getAgentCurrentPositionY()
          ][ len( self.grid_obj.grid_mtx[
            partner_obj.getAgentCurrentPositionX()
              ][ partner_obj.getAgentCurrentPositionY() ] )-1 
                ] == partner_obj

      return household_size_

    if( type( partner_age_int ) == bool ):

      agent_obj_.setAgentAsSeparated()

      self.counters_obj.increaseTotalNumOfSeparated()
      self.counters_obj.decreaseTotalNumOfMarried()

      return household_size_-1

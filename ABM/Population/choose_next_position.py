#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseNextPosition( object ):

  def chooseNextPosition(
    self,
    agent_obj_
  ):

    pos_coord_tuple = self.grid_obj.chooseRandPositionFar( 
      agent_obj_.getAgentCurrentPositionX(),
      agent_obj_.getAgentCurrentPositionY()
    )

    while(  self.grid_obj.isPositionAllowed( pos_coord_tuple[0], pos_coord_tuple[1] ) != True ):

      pos_coord_tuple = self.grid_obj.chooseRandPositionFar( 
        int( round( self.grid_obj.getGridSizeX()/2 ) ),
        int( round( self.grid_obj.getGridSizeY()/2 ) )
      )

    agent_obj_.setAgentNextPositionX(
      pos_coord_tuple[0]
    )

    agent_obj_.setAgentNextPositionY(
      pos_coord_tuple[1]
    )
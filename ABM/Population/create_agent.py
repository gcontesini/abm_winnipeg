#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Agents.agents as agents_class

class createAgent( object ):

  def createAgent(
    self,
    gender_int,
    age_index_int
  ):

    male_int = 0
    female_int = 1

    # ---------------------------------------------- Agent Object
    
    agent_obj = agents_class.Agents(
        self.counters_obj.getTotalNumOfAgents()
    )

    # ---------------------------------------------- Counters Related

    self.counters_obj.increaseTotalNumOfAgents()

    self.counters_obj.decreaseNumOfAdultsAllowed()

    self.counters_obj.decreaseNumOfAgentsForAgeGroup(
      gender_int,
      age_index_int
    )

    # ---------------------------------------------- Agent Age

    age_int = self.convertIndexToAge( age_index_int )

    agent_obj.setAgentAge( age_int )

    # ---------------------------------------------- Agent Gender

    if( gender_int == male_int ):

      agent_obj.setAgentAsMale()
      self.counters_obj.increaseTotalNumOfMales()

    elif( gender_int == female_int ):

      agent_obj.setAgentAsFemale()
      self.counters_obj.increaseTotalNumOfFemales()

    # ---------------------------------------------- Agent Ethnicity

    self.chooseEthnicity( agent_obj )
    
    # ---------------------------------------------- Agent Position

    return agent_obj

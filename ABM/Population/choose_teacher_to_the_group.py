#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import random as random

class chooseTeacherToTheGroup( object ):

  def chooseTeacherToTheGroup(
    self,
    work_group_obj_
  ):

    teacher_obj = None

    while( teacher_obj == None ):

      rand_porsiton_tpl = self.grid_obj.chooseRandPosition()

      if( self.grid_obj.isPositionAllowed( rand_porsiton_tpl[0], rand_porsiton_tpl[1] ) ):

        if( self.grid_obj.getNumberOfAgentsInPosition( rand_porsiton_tpl[0], rand_porsiton_tpl[1] ) >= 1 ):

          teacher_obj =  random.choice( 
            self.grid_obj.\
            grid_mtx[ rand_porsiton_tpl[ 0 ] ][ rand_porsiton_tpl[ 1 ] ]
          )

          if(
            teacher_obj.getAgentAge() >= 55
            or
            teacher_obj.getAgentAge() <= 25
          ):

            teacher_obj = None

    work_group_obj_.addAgentToGroup(
      teacher_obj
    )

    teacher_obj.addWorkGroupToAgent( work_group_obj_ )
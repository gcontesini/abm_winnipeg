#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class getAgentAgeGroupFromGrid( object ):

  def getAgentAgeGroupFromGrid(
    self,
    init_age_int_,
    final_age_int_
   ):

    agent_list = []

    for x_int in range ( self.grid_obj.getGridSizeX() ):
  
      for y_int in range ( self.grid_obj.getGridSizeY() ):
  
        if( self.grid_obj.isPositionAllowed(x_int,y_int) ):

          for agent_obj in self.grid_obj.grid_mtx[ x_int ][ y_int ]:

            if(
              agent_obj.getAgentAge() <= final_age_int_
              and
              agent_obj.getAgentAge() >= init_age_int_
            ):

              agent_list.append( agent_obj )

    return agent_list

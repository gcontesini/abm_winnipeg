#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Groups.groups as groups_class

class createWorkGroup( object ):

  def createWorkGroup( self ):

    adults_list = self.getAgentAgeGroupFromGrid(
      self.simulation_parameters_obj.getWorkGroupAgeGroup()[0],
      self.simulation_parameters_obj.getWorkGroupAgeGroup()[1]
    )
    
    adult_index_int = 0

    while( adult_index_int < len( adults_list ) ):

      if( adults_list[ adult_index_int ].getAgentWorkGroup() == None ):

        work_group_obj = groups_class.Groups(
          self.counters_obj.getTotalNumOfWorkGroups()
        ) 
        
        self.counters_obj.increaseTotalNumOfWorkGroups()

        work_group_obj.setGroupAsWork()

        # ------------------------------------------------------- Group Position

        work_position_tuple = self.grid_obj.chooseRandPosition()

        work_group_obj.setGroupPositionX( work_position_tuple[0] )
        work_group_obj.setGroupPositionY( work_position_tuple[1] )

        # ----------------------------------------------------------- Group Size
        
        group_size_int = \
          self.simulation_parameters_obj.getWorkGroupSize() + \
          int(
            round(
              self.normalDistribution(
                self.simulation_parameters_obj.getWorkGroupMean(),
                self.simulation_parameters_obj.getWorkGroupSD()
              )
            )
          )

        if( group_size_int > ( len( adults_list ) - adult_index_int ) ):

          group_size_int = ( len( adults_list ) - adult_index_int )

        # ----------------------------------------------------- Add First Worker

        adults_list[ adult_index_int ].addWorkGroupToAgent( 
          work_group_obj 
        )

        work_group_obj.addAgentToGroup( 
          adults_list[ adult_index_int ] 
        )

        # -------------------------------------------------- Add Rest of Workers

        adult_index_int = adult_index_int + 1
        
        while( work_group_obj.getNumberOfMembersInTheGroup() < group_size_int ):
          
          if( adult_index_int >= len( adults_list )):

            break

          else:

            if( adults_list[ adult_index_int ].getAgentWorkGroup() == None ):

              adults_list[ adult_index_int ].\
                addWorkGroupToAgent( work_group_obj )

              work_group_obj.\
                addAgentToGroup( adults_list[ adult_index_int ] )

            adult_index_int = adult_index_int + 1

      else:

        adult_index_int = adult_index_int + 1
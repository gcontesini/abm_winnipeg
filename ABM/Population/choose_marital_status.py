#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseMaritalStatus( object ):

  def chooseMaritalStatus(
    self,
    agent_obj_,
    household_size_int_
  ):

    rand_num_int = self.getRandomNumberDefault()

    if( household_size_int_ > 1 ):

      if(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfBeingSingle()
      ):

        agent_obj_.setAgentAsSingle()
        self.counters_obj.increaseTotalNumOfSingle()

      elif(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfBeingMarried()
      ):

        agent_obj_.setAgentAsMarried()
        self.counters_obj.increaseTotalNumOfMarried()

      elif(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfBeingWidowed()
        ):

        agent_obj_.setAgentAsSeparated()
        self.counters_obj.increaseTotalNumOfSeparated() 

    elif( household_size_int_ == 1 ):

      if(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfBeingSingle()*\
        1E5 /\
        (
          self.simulation_parameters_obj.getProbabilityOfBeingSingle()+\
          (
            self.simulation_parameters_obj.getProbabilityOfBeingMarried()-\
            self.simulation_parameters_obj.getProbabilityOfBeingSingle()
          )/2
        )
      ):

        agent_obj_.setAgentAsSingle()
        self.counters_obj.increaseTotalNumOfSingle()

      elif(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfBeingWidowed()*\
        1E5 /\
        (
          self.simulation_parameters_obj.getProbabilityOfBeingWidowed()-\
          self.simulation_parameters_obj.getProbabilityOfBeingMarried()+\
          (
            self.simulation_parameters_obj.getProbabilityOfBeingMarried()-\
            self.simulation_parameters_obj.getProbabilityOfBeingSingle()
          )/2
        )
        ):

        agent_obj_.setAgentAsSeparated()
        self.counters_obj.increaseTotalNumOfSeparated() 

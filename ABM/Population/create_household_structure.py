#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class createHouseholdStructure( object ):

  def createHouseholdStructure(
    self,
    agent_obj_,
    household_size_
  ):

    household_size_int = household_size_

    if( agent_obj_.getAgentMaritalStatus() == "single" ):

      if(
        agent_obj_.getAgentAge() >= 70
        or
        agent_obj_.getAgentAge() <= 25
      ):

        household_size_int = 2

      else:
        
        household_size_int = self.createChildren( agent_obj_, household_size_int )

    elif( agent_obj_.getAgentMaritalStatus() == 'married' ):

      household_size_int = self.createPartner( agent_obj_, household_size_int )

      if(
        agent_obj_.getAgentAge() >= 70
        or
        agent_obj_.getAgentAge() <= 25
      ):

        household_size_int = 2

      else:
        
        household_size_int = self.createChildren( agent_obj_, household_size_int )

    elif( agent_obj_.getAgentMaritalStatus() == "separated" ):

      if(
        agent_obj_.getAgentAge() >= 70
        or
        agent_obj_.getAgentAge() <= 25
      ):

        household_size_int = 1

      else:

        household_size_int = self.createChildren( agent_obj_, household_size_int )

    #-----------------------------------------------------------Counters Related
    
    final_family_size_int = \
      agent_obj_.getAgentFamilyGroup().getNumberOfMembersInTheGroup()
    
    if(
      final_family_size_int == 1
    ):

      final_index_int = 0

    elif(
      final_family_size_int == 2
    ):

      final_index_int = 1

    elif(
      final_family_size_int == 3
    ):

      final_index_int = 2

    elif(
      final_family_size_int <= 5
    ):

      final_index_int = 3

    elif(
      final_family_size_int <= 8
    ):

      final_index_int = 4

    if(
      household_size_int == 1
    ):

      original_index_int = 0

    elif(
      household_size_int == 2
    ):

      original_index_int = 1

    elif(
      household_size_int == 3
    ):

      original_index_int = 2

    elif(
      household_size_int <= 5
    ):

      original_index_int = 3

    elif(
      household_size_int <= 8
    ):

      original_index_int = 4

    self.counters_obj.increaseNumOfHouseholdSizeAllowed( original_index_int )
    self.counters_obj.decreaseNumOfHouseholdSizeAllowed( final_index_int )
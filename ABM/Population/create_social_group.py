#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Groups.groups as groups_class

class createSocialGroup( object ):

  def createSocialGroup( self ):
  
    agent_list = self.getAgentAgeGroupFromGrid(
      self.simulation_parameters_obj.getSocialGroupAgeGroup()[0],
      self.simulation_parameters_obj.getSocialGroupAgeGroup()[1]
    )
    agent_list = self.sortAgentList( agent_list )
    
    agent_index_int = 0

    while( agent_index_int < len( agent_list ) ):

      if( agent_list[ agent_index_int ].getAgentSocialGroup() == None ):

        social_group_obj = groups_class.Groups(
          self.counters_obj.getTotalNumOfSocialGroups()
        )

        self.counters_obj.increaseTotalNumOfSocialGroups()

        social_group_obj.setGroupAsSocial()

        # ------------------------------- Group Position

        group_position_tuple = self.grid_obj.chooseRandPosition()

        social_group_obj.setGroupPositionX( group_position_tuple[0] )
        social_group_obj.setGroupPositionY( group_position_tuple[1] )

        # ----------------------------------- Group Size

        group_size_int = \
          self.simulation_parameters_obj.getSocialGroupSize() + \
            int(
              round(
                self.normalDistribution(
                  self.simulation_parameters_obj.getSocialGroupMean(),
                  self.simulation_parameters_obj.getSocialGroupSD()
                )
              )
            )

        if( group_size_int > ( len( agent_list ) - agent_index_int ) ):

          group_size_int = ( len( agent_list ) - agent_index_int )

        # ---------------------------- Add First Friend

        agent_list[ agent_index_int ].\
          addSocialGroupToAgent( social_group_obj )

        social_group_obj.\
          addAgentToGroup(
            agent_list[ agent_index_int ]
          )

        # ------------------------- Add Rest of Students

        agent_index_int = agent_index_int + 1

        while( social_group_obj.getNumberOfMembersInTheGroup() < group_size_int ):

          if( agent_list[ agent_index_int ].getAgentSocialGroup() == None ):
          
            agent_list[ agent_index_int ].\
              addSocialGroupToAgent( social_group_obj )

            social_group_obj.\
              addAgentToGroup( agent_list[ agent_index_int ] )

          agent_index_int = agent_index_int + 1
          
      else:

        agent_index_int = agent_index_int + 1

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseAdultAge( object ):
 
  def chooseAdultAge(
    self,
    gender_int_
  ):
    # gender_int_: 
    #   male (0)
    #   female (1)

    index_int = self.chooseListRandomIndex(
      self.simulation_parameters_obj.
        getAgeGroupProbabilitiesPerGender( gender_int_ )
    )

    if(
        self.counters_obj.getMaxNumOfAgentsForAgeGroup(
          gender_int_,
          index_int
      ) == 0 or
      index_int <= 3
      # or
      # self.convertIndexToAge(index_int) < 18
    ):
    
      return False

    return index_int	
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Agents.agents as agents_class

class createAdults( object ):

  def createAdults(
    self,
    agent_obj_,
    household_size_
  ):

    male_int = 0
    female_int = 1

    num_of_roommates = (
      household_size_ -
      agent_obj_.getAgentFamilyGroup().getNumberOfMembersInTheGroup()
    )

    while( 
      num_of_roommates > 0
      and 
      self.counters_obj.getMaxNumOfAdultsAllowed() > 0
      and
      self.counters_obj.getTotalNumOfAgents()
      <=
      self.simulation_parameters_obj.getPopulationSize()
    ):

      gender_int = self.chooseGender( )
      
      adult_age_index_int = self.chooseAdultAge( gender_int )

      if( type( adult_age_index_int ) == int ):

        # --------------------------------------------------- Adult Object

        adult_obj = agents_class.Agents(
          self.counters_obj.getTotalNumOfAgents()
        )
        
        # --------------------------------------------------- Counters Related

        self.counters_obj.increaseTotalNumOfAgents()

        self.counters_obj.decreaseNumOfAdultsAllowed()

        adult_obj.setAgentAsSingle()
        
        self.counters_obj.increaseTotalNumOfSingle()

        num_of_roommates = num_of_roommates - 1 

        # --------------------------------------------------- Adult Family


        agent_obj_.getAgentFamilyGroup().addAgentToGroup( adult_obj )        

        adult_obj.addFamilyGroupToAgent( agent_obj_.getAgentFamilyGroup() )

        # --------------------------------------------------- Adult Grid Related

        adult_obj.setAgentCurrentPositionX( adult_obj.getAgentFamilyGroup().getGroupPositionX() )
        adult_obj.setAgentCurrentPositionY( adult_obj.getAgentFamilyGroup().getGroupPositionY() )

        self.grid_obj.addAgentToPosition(
          adult_obj.getAgentFamilyGroup().getGroupPositionX(),
          adult_obj.getAgentFamilyGroup().getGroupPositionY(),
          adult_obj
        )     

        self.chooseNextPosition( adult_obj )

        # assert agent_obj_.getAgentCurrentPositionX() == adult_obj.getAgentCurrentPositionX()
        # assert agent_obj_.getAgentCurrentPositionY() == adult_obj.getAgentCurrentPositionY() 
        # assert self.grid_obj.grid_mtx[ 
        #   agent_obj_.getAgentCurrentPositionX()
        #   ][ agent_obj_.getAgentCurrentPositionY()
        #     ][ len( self.grid_obj.grid_mtx[
        #       agent_obj_.getAgentCurrentPositionX()
        #         ][ agent_obj_.getAgentCurrentPositionY() ] )-1 
        #           ] == adult_obj

        # --------------------------------------------------- Adult Age

        adult_age_int = self.convertIndexToAge( adult_age_index_int )

        adult_obj.setAgentAge( adult_age_int )  

        self.counters_obj.decreaseNumOfAgentsForAgeGroup(
          gender_int,
          adult_age_index_int
        )

        # ------------------------------------------------------- Adult Gender

        if( gender_int == male_int ):

          adult_obj.setAgentAsMale()
          
          self.counters_obj.increaseTotalNumOfMales()

        elif( gender_int == female_int ):

          adult_obj.setAgentAsFemale()

          self.counters_obj.increaseTotalNumOfFemales()

        self.chooseEthnicity( adult_obj )

        # ------------------------------------------------------ Adult Grid Related

      if( type( adult_age_index_int ) == bool ):

        num_of_roommates = num_of_roommates - 1
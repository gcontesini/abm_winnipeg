#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class sortAgentList( object ):

	def sortAgentList(
		self,
		initial_list_
	):

		result_list = []
		aux_dic = {}

		for agent_obj in initial_list_:

			try: 
				aux_dic[ agent_obj.getAgentAge()  ].append( agent_obj ) 

			except:
				aux_dic[ agent_obj.getAgentAge() ] = []
				aux_dic[ agent_obj.getAgentAge()  ].append( agent_obj ) 

		for k_keys in aux_dic.keys() :
			for agent_obj in aux_dic[ k_keys ] :
				result_list.append( agent_obj )

		return result_list
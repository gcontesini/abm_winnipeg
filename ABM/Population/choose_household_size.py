#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import numpy as np

class chooseHouseholdSize( object ):

  def chooseHouseholdSize(
    self,
    agent_obj_
  ):

    rand_num_int = self.getRandomNumberDefault()

    if(
      rand_num_int
      <=
      self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[0]
    ):

      index_int = 0
      household_size_int = 1

    elif(
      rand_num_int
      <=
      self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[1]
    ):
    
      index_int = 1
      household_size_int = 2

    elif(
      rand_num_int
      <=
      self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[2]
    ):

      index_int = 2
      household_size_int = 3

    elif(
      rand_num_int
      <=
      self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[3]
    ):    

      index_int = 3
      household_size_int = np.random.random_integers(4,5)

    elif(
      rand_num_int
      <=
      self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[4]
    ):    

      index_int = 4
      household_size_int =  np.random.random_integers(6,8)
      
    while( 
      self.counters_obj.getTotalNumOfHouseholdSize( index_int ) >=\
      self.counters_obj.getMaxNumOfHouseholdSizeAllowed( index_int )  
    ):

      rand_num_int = self.getRandomNumberDefault()

      if(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[0]
      ):

        index_int = 0
        household_size_int = 1

      elif(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[1]
      ):
      
        index_int = 1
        household_size_int = 2

      elif(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[2]
      ):

        index_int = 2
        household_size_int = 3

      elif(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[3]
      ):    

        index_int = 3
        household_size_int = np.random.random_integers(4,5)

      elif(
        rand_num_int
        <=
        self.simulation_parameters_obj.getProbabilityOfHouseholdSize()[4]
      ):    

        index_int = 4
        household_size_int =  np.random.random_integers(6,8)
        
    self.counters_obj.decreaseNumOfHouseholdSizeAllowed( index_int )
    return household_size_int
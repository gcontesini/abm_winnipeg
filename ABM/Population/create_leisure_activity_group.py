#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Groups.groups as groups_class

class createLeisureActivityGroup( object ):

  def createLeisureActivityGroup( self ):
  
    agent_list = self.getAgentAgeGroupFromGrid(
      self.simulation_parameters_obj.getLeisureActivityGroupAgeGroup()[0],
      self.simulation_parameters_obj.getLeisureActivityGroupAgeGroup()[1]
    )
    agent_list = self.sortAgentList( agent_list )

    agent_index_int = 0

    while( agent_index_int < len( agent_list ) ):

      if( agent_list[ agent_index_int ].getAgentLeisureActivityGroup() == None ):
        
        leisure_activity_group_obj = groups_class.Groups(
          self.counters_obj.getTotalNumOfSocialGroups()
        )

        self.counters_obj.increaseTotalNumOfLeisureActivityGroups()

        leisure_activity_group_obj.setGroupAsLeisureActivity()

        # ------------------------------- Group Position

        group_position_tuple = self.grid_obj.chooseRandPosition()

        leisure_activity_group_obj.setGroupPositionX( group_position_tuple[0] )
        leisure_activity_group_obj.setGroupPositionY( group_position_tuple[1] )

        # ----------------------------------- Group Size

        group_size_int = \
          self.simulation_parameters_obj.getLeisureActivityGroupSize() + \
          int(
            round(
              self.normalDistribution(
                self.simulation_parameters_obj.getLeisureActivityGroupMean(),
                self.simulation_parameters_obj.getLeisureActivityGroupSD()
              )
            )
          )

        if( group_size_int > ( len( agent_list ) - agent_index_int ) ):

          group_size_int = ( len( agent_list ) - agent_index_int )

        # ---------------------------- Add First Friend

        agent_list[ agent_index_int ].\
          addLeisureActivityGroupToAgent( leisure_activity_group_obj )

        leisure_activity_group_obj.\
          addAgentToGroup(
            agent_list[ agent_index_int ]
          )

        # ------------------------- Add Rest of Students

        agent_index_int = agent_index_int + 1

        while( leisure_activity_group_obj.getNumberOfMembersInTheGroup() < group_size_int ):

          if( agent_index_int >= len( agent_list )):

            break

          else:

            if( agent_list[ agent_index_int ].getAgentLeisureActivityGroup() == None ):

              agent_list[ agent_index_int ].\
                addLeisureActivityGroupToAgent( leisure_activity_group_obj )

              leisure_activity_group_obj.\
                addAgentToGroup( agent_list[ agent_index_int ] )

              agent_index_int = agent_index_int + 1

            else:

              agent_index_int = agent_index_int + 1

        #------------------------------------------------------------------------Including Dads 

        age_average_float = 0.

        for i_int in range( leisure_activity_group_obj.getNumberOfMembersInTheGroup() ):

          age_average_float = age_average_float +\
          leisure_activity_group_obj.getAgent( i_int ).getAgentAge()

        age_average_float = age_average_float /\
          leisure_activity_group_obj.getNumberOfMembersInTheGroup()

        if( age_average_float < 15 ):
          
          for dad_index_int in range( leisure_activity_group_obj.getNumberOfMembersInTheGroup() ):

            leisure_activity_group_obj.\
              getAgent( dad_index_int ).\
                getAgentFamilyGroup().\
                  getAgent( 0 ).\
                    addLeisureActivityGroupToAgent( leisure_activity_group_obj )

            leisure_activity_group_obj.\
              addAgentToGroup( 
                leisure_activity_group_obj.\
                  getAgent( dad_index_int ).\
                    getAgentFamilyGroup().\
                      getAgent(0)
              )

      else:

        agent_index_int = agent_index_int + 1


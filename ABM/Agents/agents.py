#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from agent_methods import   *

class Agents(

  # agent related #
  
  getAgentAge,
  getAgentCurrentPositionX,
  getAgentCurrentPositionY,
  getAgentEthnicity,
  getAgentGender,
  getAgentID,
  getAgentMaritalStatus,
  getAgentNextPositionX,
  getAgentNextPositionY,
  getAgentProperTime,
  increaseAgentProperTime,
  setAgentAge,
  setAgentAsAboriginal,
  setAgentAsFemale,
  setAgentAsKid,
  setAgentAsMale,
  setAgentAsMarried,
  setAgentAsSeparated,
  setAgentAsNonAboriginal,
  setAgentAsSingle,
  setAgentCurrentPositionX,
  setAgentCurrentPositionY,
  setAgentNextPositionX,
  setAgentNextPositionY,
  showAgentEthnicity,
  showAgentGender,
  showAgentMaritalStatus,

  # group related #

  addFamilyGroupToAgent,
  addWorkGroupToAgent,
  addSocialGroupToAgent,
  addLeisureActivityGroupToAgent,
  getAgentFamilyGroup,
  getAgentWorkGroup,
  getAgentSocialGroup,
  getAgentLeisureActivityGroup,


  # activity related #

  Activity,

  # getAgentDiseaseStatus,
  Disease
):

#--------------------------------------------------------------------Constructor
  def __init__(
    self,
    uid_int_
  ):

    # --------------------------------------------------------------- Attributes

    #~~# Agent Profile #~~#

    self.aboriginal_bool = None

    self.age_int = None
    self.gender_int = None
    self.proper_time_int = 0

    self.marital_status_str = None

    #~~# Grid Related #~~#

    self.current_position_x_int = None
    self.current_position_y_int = None
    self.next_position_x_int = None
    self.next_position_y_int = None

    #~~# Disease Related #~~#

    self.disease_status_obj = None

    #~~# Group Related #~~#

    self.family_group_obj = None
    self.work_group_obj = None
    self.social_group_obj = None
    self.leisure_activity_group_obj = None

    #~~# Schedule Related #~~#

    self.activity_status_obj = None

    #~~# Sets #~~#

    self.id_int = uid_int_

#----------------------------------------------------------------------------End

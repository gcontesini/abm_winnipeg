#!/usr/bin/python

import numpy
from distutils.core import setup 
from Cython.Build import cythonize

ext_modules=[
    Extension("agent",       ["ABM/Agents/*.pyx"]),
]

setup(
  name='main_in_c',
  ext_modules = cythonize('main.pyx',
  include_path = [...])
  )
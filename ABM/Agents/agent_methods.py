#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

# agent related #

from get_agent_age                              import  *
from get_agent_current_position_x               import  *
from get_agent_current_position_y               import  *
from get_agent_ethnicity                        import  *
from get_agent_gender                           import  *
from get_agent_id                               import  *
from get_agent_marital_status                   import  *
from get_agent_next_position_x                  import  *
from get_agent_next_position_y                  import  *
from increase_agent_proper_time                 import  *
from set_agent_age                              import  *
from set_agent_as_aboriginal                    import  *
from set_agent_as_kid                           import  *
from set_agent_as_male                          import  *
from set_agent_as_married                       import  *
from set_agent_as_separated 					import 	*
from set_agent_as_non_aboriginal                import  *
from set_agent_current_position_x               import  *
from set_agent_current_position_y               import  *
from set_agent_next_position_x                  import  *
from set_agent_next_position_y                  import  *
from show_agent_ethnicity                       import  *
from show_agent_gender                          import  *
from show_agent_marital_status                  import  *
from set_agent_as_female                        import  *
from set_agent_as_separated                     import  *
from set_agent_as_single                        import  *

# group related #
from add_family_group_to_agent                  import  *
from add_work_group_to_agent                    import  *
from add_leisure_activity_group_to_agent		import	*
from add_social_group_to_agent					import	*
from get_agent_family_group                     import  *
from get_agent_leisure_activity_group           import  *
from get_agent_social_group                     import  *
from get_agent_proper_time                      import  *
from get_agent_work_group                       import  *

# activity related #
from ABM.Activity.activity                      import  *

# disease related #
from ABM.Disease.disease 						import	*

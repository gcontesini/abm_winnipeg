#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from disease_logic_methods import*

class chooseInfectedDuration( object ):

  def chooseInfectedDuration( self ):

    infected_duration_int = \
      int(
        round(
          24*self.logNormalDistribution(
            self.simulation_parameters_obj.getInfectedMean(),
            self.simulation_parameters_obj.getInfectedSd()
          )
        )
      )

    while( 
      infected_duration_int < self.simulation_parameters_obj.getInfectedMinDuration() or
      infected_duration_int > self.simulation_parameters_obj.getInfectedMaxDuration() 
    ):
      
      infected_duration_int = \
        int(
          round(
            24*self.logNormalDistribution(
              self.simulation_parameters_obj.getInfectedMean(),
              self.simulation_parameters_obj.getInfectedSd()
            )
          )
        )
    return infected_duration_int
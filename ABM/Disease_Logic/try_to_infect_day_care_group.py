#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> and \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class tryToInfectDayCareGroup( object ):

  def tryToInfectDayCareGroup(
    self,
    agent_obj_,
    simulation_parameters_obj_,
    simulation_counters_obj_
  ):

    possible_number_of_interactions_int = \
    int(
      abs(
        # ( simulation_parameters_obj_.getPercentageOfDayCareInteractions() / 100000. )*\
        ( agent_obj_.getAgentWorkGroup().getNumberOfMembersInTheGroup() - 1 ) #Minus the infected one
      )
    )

    number_of_susceptible_agents_in_the_group = \
      self.getNumberOfSusceptibleAgentsInTheGroup(
        agent_obj_.getAgentWorkGroup()
      )

    while( 
      number_of_susceptible_agents_in_the_group > 0
      and possible_number_of_interactions_int > 0
    ):

      susceptible_agent_obj = \
        self.chooseSusceptibleAgentInTheGroup(
          agent_obj_.getAgentWorkGroup()
        )

      if( susceptible_agent_obj.isAgentAtDayCare() == True ):

        if( self.getRandomNumberDefault() <=\
          simulation_parameters_obj_.getProbabilityOfInfection()
        ):

          susceptible_agent_obj.setDiseaseStateAsExposed()

          simulation_counters_obj_.increaseNumberOfExposed()
          simulation_counters_obj_.decreaseNumberOfSusceptible()

          exposed_duration_int = self.chooseExposedDuration()

          susceptible_agent_obj.setDiseaseStateTime( exposed_duration_int )
          
          susceptible_agent_obj.setDiseaseSourceUID(
              agent_obj_.getAgentID()
          )

          susceptible_agent_obj.setDiseaseSourcePlaceX(
              susceptible_agent_obj.getAgentCurrentPositionX()
          )

          susceptible_agent_obj.setDiseaseSourcePlaceY(
              susceptible_agent_obj.getAgentCurrentPositionY()
          )

          susceptible_agent_obj.setDiseaseSourceActivity(
              susceptible_agent_obj.getAgentActivity()
          )
          
          number_of_susceptible_agents_in_the_group = \
            number_of_susceptible_agents_in_the_group - 1

        possible_number_of_interactions_int = \
          possible_number_of_interactions_int - 1
      
      else :

        possible_number_of_interactions_int = \
          possible_number_of_interactions_int - 1
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> and \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseSusceptibleAgentInTheGroup( object ):

  def chooseSusceptibleAgentInTheGroup(
    self,
    group_obj_
  ):
    control_bool = False

    while( control_bool == False ):

      random_agent_index_int = self.getRandomNumber(
        0,
        group_obj_.getNumberOfMembersInTheGroup()
      )

      possible_agent_obj = group_obj_.getAgent( random_agent_index_int )

      if( possible_agent_obj.isDiseaseStateSusceptible() == True ):

        control_bool = True
      
    return possible_agent_obj
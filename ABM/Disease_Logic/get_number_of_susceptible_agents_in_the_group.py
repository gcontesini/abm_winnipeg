#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> and \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class getNumberOfSusceptibleAgentsInTheGroup( object ):

  def getNumberOfSusceptibleAgentsInTheGroup(
    self,
    group_obj_
  ):

    number_of_susceptible_int = 0 

    for index_int in range( 0, group_obj_.getNumberOfMembersInTheGroup(), 1):

      agent_obj_ = group_obj_.getAgent( index_int )

      if( agent_obj_.isDiseaseStateSusceptible() == True ):

        number_of_susceptible_int = number_of_susceptible_int + 1

    return number_of_susceptible_int

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class tryToRecover( object ):

  def tryToRecover(
    self                      ,
    agent_obj_                ,
    simulation_counters_obj_  ,
    simulation_parameters_obj_
  ):

    if( agent_obj_.getDiseaseStateTime() == 0 ):

      if( agent_obj_.isDiseaseStateExposed() ):

        agent_obj_.setDiseaseStateAsInfected()

        simulation_counters_obj_.increaseNumberOfInfected()
        simulation_counters_obj_.decreaseNumberOfExposed()

        infected_duration_int = self.chooseInfectedDuration()

        agent_obj_.setDiseaseStateTime( infected_duration_int )

        if( agent_obj_.isDiseaseDiagnosed() == False ):

          if( self.getRandomNumberDefault() <= simulation_parameters_obj_.getProbabilityOfDiseaseDiagnostic() ):

              agent_obj_.setDiseaseDiagnosticAsTrue()

      elif( agent_obj_.isDiseaseStateInfected() ):

        agent_obj_.setDiseaseStateAsRecovered()
        simulation_counters_obj_.decreaseNumberOfInfected()
        simulation_counters_obj_.increaseNumberOfRecovered()

    else:

      agent_obj_.decreaseDiseaseStateTime()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import math as math

class chooseInitialCases( object ):

  def chooseInitialCases(
    self,
    simulation_counters_obj_,
    demographic_counters_obj_,
    simulation_parameters_obj_
  ):

    i_int = 0

    while(
      i_int <\
      self.simulation_parameters_obj.getDiseaseInitialCases()
    ):
      
      position_tpl = self.grid_obj.chooseRandPosition()

      if(
        self.grid_obj.isPositionAllowed( position_tpl[0], position_tpl[1] )
      ):

        if(
          self.grid_obj.getNumberOfAgentsInPosition( position_tpl[0], position_tpl[1] ) > 0
        ):

          lucky_one_int = \
            self.grid_obj.getRandomNumber(
              0,
              self.grid_obj.getNumberOfAgentsInPosition( position_tpl[0],position_tpl[1] )
            )

          agent_obj = self.grid_obj.\
            getGrid()\
              [position_tpl[0]]\
              [position_tpl[1]]\
              [lucky_one_int]

          if( agent_obj.isDiseaseStateSusceptible() ):

            agent_obj.setDiseaseStateAsExposed()

            simulation_counters_obj_.increaseNumberOfExposed()
            simulation_counters_obj_.decreaseNumberOfSusceptible()

            exposed_duration_int = self.chooseExposedDuration()

            agent_obj.setDiseaseStateTime( exposed_duration_int )

            agent_obj.setDiseaseSourceUID( "000" )
            agent_obj.setDiseaseSourcePlaceX(0)
            agent_obj.setDiseaseSourcePlaceX(0)
            agent_obj.setDiseaseSourceActivity("initial_case")
            i_int = i_int + 1

            # if( self.getRandomNumberDefault() <= simulation_parameters_obj_.getProbabilityOfDiseaseDiagnostic() ):

            #   agent_obj.setDiseaseDiagnosticAsTrue()

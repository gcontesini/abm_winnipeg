#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> and \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import random
# import numpy.random as np

class tryToInfectRandomGroup( object ):

  def tryToInfectRandomGroup(
    self,
    agent_obj_,
    simulation_parameters_obj_,
    simulation_counters_obj_,
    grid_obj_
  ):

    number_of_agents_in_position_int = \
      grid_obj_.getNumberOfAgentsInPosition(
        agent_obj_.getAgentCurrentPositionX(),
        agent_obj_.getAgentCurrentPositionY()
      ) - 1 # Minus the infected agent

    possible_number_of_interactions_int = \
    abs(
      int(
        # ( simulation_parameters_obj_.getPercentageOfRandomInteractions()/ 100000 )*\
        number_of_agents_in_position_int
      )
    )

    grid_mtx = grid_obj_.getGrid()

    agents_in_position_lst = []

    number_of_susceptible_int = 0

    number_of_agents_in_position_int = \
      grid_obj_.getNumberOfAgentsInPosition(
        agent_obj_.getAgentCurrentPositionX(),
        agent_obj_.getAgentCurrentPositionY()
      ) - 1 # Minus the infected agent

    for index_int in range( number_of_agents_in_position_int ):

      random_agent_obj = \
      grid_mtx[ agent_obj_.getAgentCurrentPositionX() ][ agent_obj_.getAgentCurrentPositionY()][index_int]

      if( agent_obj_.isDiseaseStateSusceptible() ):

        if(random_agent_obj.isAgentAtRandom() ):

          possible_interaction_list.append(random_agent_obj)

          number_of_susceptible_int = number_of_susceptible_int + 1

    while( possible_number_of_interactions_int > 0 and number_of_susceptible_int > 0 ):

      luck_agent_obj = random.choice( agents_in_position_list )

      if(
        self.getRandomNumberDefault() <=\
          simulation_parameters_obj_.getProbabilityOfInfection()
      ):
        
        luck_agent_obj.setDiseaseStateAsExposed()

        simulation_counters_obj_.increaseNumberOfExposed()
        simulation_counters_obj_.decreaseNumberOfSusceptible()

        exposed_duration_int = self.chooseExposedDuration()
        luck_agent_obj.setDiseaseStateTime( exposed_duration_int )

        luck_agent_obj.setDiseaseSourceUID(
          agent_obj_.getAgentID()
        )
        luck_agent_obj.setDiseaseSourcePlaceX(
          luck_agent_obj.getAgentCurrentPositionX()
        )
        luck_agent_obj.setDiseaseSourcePlaceY(
          luck_agent_obj.getAgentCurrentPositionY()
        )
        luck_agent_obj.setDiseaseSourceActivity(
          luck_agent_obj.getAgentActivity()
        )

        number_of_susceptible_int = number_of_susceptible_int - 1

      possible_number_of_interactions_int = possible_number_of_interactions_int - 1
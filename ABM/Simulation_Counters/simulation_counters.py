#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from simulation_counters_methods import*

class SimulationCounters(
decreaseNumberOfExposed     ,
decreaseNumberOfInfected    ,
decreaseNumberOfRecovered   ,
decreaseNumberOfSusceptible ,
getDay                      ,
getDayOfTheWeek             ,
getHour                     ,
getMonth                    ,
getNumberOfExposed          ,
getNumberOfInfected         ,
getNumberOfRecovered        ,
getNumberOfSusceptible      ,
getSimulationDay            ,
getSimulationDuration       ,
getSimulationHour           ,
getYear                     ,
increaseDay                 ,
increaseDayOfTheWeek        ,
increaseHour                ,
increaseMonth               ,
increaseNumberOfExposed     ,
increaseNumberOfInfected    ,
increaseNumberOfRecovered   , 
increaseNumberOfSusceptible ,
increaseSimulationHour      ,
increaseYear                ,
setSimulationDuration       ,
isWeekend                          
):

# ------------------------------------------------------------------ Constructor

  def __init__( self ):

    #---------------------------------------------------------------- Parameters

    self.simulation_duration_int = None

    #------------------------------------------------------------------ Counters

    #~~# Disease Related #~~#

    self.number_of_susceptible_int  = 0
    self.number_of_exposed_int      = 0
    self.number_of_infected_int     = 0
    self.number_of_recovered_int    = 0

    #~~# Time Related #~~#

    self.day_int = 1
    self.day_of_the_week_int = 1
    self.simulation_days_int = 1
    self.hour_int = 0
    self.simulation_hour_int = 0
    self.month_int = 1
    self.year_int = 1

# -------------------------------------------------------------------------- END
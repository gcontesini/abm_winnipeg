#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from activity_methods import*

class Activity(
    decreaseAgentActivityDuration,
    getAgentActivity,
    getAgentActivityDuration,
    isAgentAtHome,
    isAgentAtDayCare,
    isAgentAtLeisureActivity,
    isAgentAtRandom,
    isAgentAtSchool,
    isAgentAtSocial,
    isAgentAtWork,
    setAgentActivityAsHome,
    setAgentActivityAsDayCare,
    setAgentActivityAsLeisureActivity,
    setAgentActivityAsRandom,
    setAgentActivityAsSchool,
    setAgentActivityAsSocial,
    setAgentActivityAsWork,
    setAgentActivityDuration

):

  # This is an abstract class so it has no constructor {__init__() } and nether self

    activity_str = "home"
    activity_duration_time_int = 6

#----------------------------------------------------------------------------End

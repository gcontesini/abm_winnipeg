#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from decrease_agent_activity_duration          import*
from get_agent_activity                        import*
from get_agent_activity_duration               import*
from is_agent_at_home                          import*
from is_agent_at_day_care                      import*
from is_agent_at_leisure_activity              import*
from is_agent_at_random                        import*
from is_agent_at_school                        import*
from is_agent_at_social                        import*
from is_agent_at_work                          import*
from set_agent_activity_as_home                import*
from set_agent_activity_as_day_care            import*
from set_agent_activity_as_leisure_activity    import*
from set_agent_activity_as_random              import*
from set_agent_activity_as_school              import*
from set_agent_activity_as_social              import*
from set_agent_activity_as_work                import*
from set_agent_activity_duration               import*

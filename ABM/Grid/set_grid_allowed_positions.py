#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import numpy as numpy_module
import xml.etree.ElementTree as ET

class setGridAllowedPositions( object ):

  def setGridAllowedPositions( 
    self,
    grid_file_str_ = False
  ):

    self.allowed_area_mtx = numpy_module.zeros(
      (
        self.grid_size_x_int,
        self.grid_size_y_int
      ), 
      dtype = object
    )

    if( type( grid_file_str_ ) == bool ):

      for i_int in range( self.grid_size_x_int ):
        
        for j_int in range( self.grid_size_y_int ):
          
          if( i_int == 0 ):
          
            self.allowed_area_mtx[ i_int ][ j_int ] = 0
          
          elif( i_int == self.grid_size_x_int-1 ):
          
            self.allowed_area_mtx[ i_int ][ j_int ] = 0
          
          elif( j_int == 0 ):
          
            self.allowed_area_mtx[ i_int ][ j_int ] = 0
          
          elif( j_int == self.grid_size_y_int - 1 ):

            self.allowed_area_mtx[ i_int ][ j_int ] = 0

          else:
            self.allowed_area_mtx[ i_int ][ j_int ] = 1 
    
    elif( type( grid_file_str_ ) == str ):

      tree = ET.parse( grid_file_str_ )

      root = tree.getroot()

      map_text = root.find("map").text

      index_int = 0

      for y_int in range( self.grid_size_y_int ):

        for x_int in range( self.grid_size_x_int ):

          self.allowed_area_mtx[ x_int ][ y_int ] = int( map_text.split()[index_int] )

          index_int = index_int + 1
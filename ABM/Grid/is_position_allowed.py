#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import numpy as numpy_module

class isPositionAllowed( object ):

  def isPositionAllowed(
    self,
    pos_x_int_,
    pos_y_int_
  ):
    try:

      if( pos_x_int_ >= 0 and pos_x_int_ <= self.grid_size_x_int ):

        if( pos_y_int_ >= 0 and pos_y_int_ <= self.grid_size_y_int ):

          if( self.allowed_area_mtx[ pos_x_int_ ][ pos_y_int_ ] != 0 ):
          
            return True       

      else:

        return False

    except IndexError:

        return False
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import numpy as numpy_module

class chooseRandPositionNear( object ):

  def chooseRandPositionNear( 
    self,
    pos_x_int_,
    pos_y_int_
  ):

    new_pos_x_int = -1
    new_pos_y_int = -1

    while( self.isPositionAllowed( new_pos_x_int, new_pos_y_int ) != True ):

      new_pos_x_int = \
        pos_x_int_ + \
        int( 
          pow(-1, ( self.getRandomNumber(1,11) % 2) ) *\
          float(self.getRandomNumber(1,11) % 3 )
        )

      new_pos_y_int = \
        pos_y_int_ + \
        int(
          pow(-1, ( self.getRandomNumber(1,11) % 2) ) *\
          float(self.getRandomNumber(1,11) % 3 )
        ) 

    return ( new_pos_x_int, new_pos_y_int )
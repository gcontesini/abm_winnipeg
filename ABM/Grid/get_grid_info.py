#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import numpy as numpy_module
import sys
import os as os

class getGridInfo( object ):

  def getGridInfo( 
    self,
    output_filename_str_ = False
  ):
    
    if( type( output_filename_str_ ) == str ):

      grid_file = open( os.path.join( "Data/"+str(output_filename_str_) ), "w" )

      total_num_int = 0

      for y_int in range( self.getGridSizeY() ):

        for x_int in range( self.getGridSizeX() ):

          if( self.isPositionAllowed( x_int, y_int ) ):

            grid_file.write( str( self.getNumberOfAgentsInPosition( x_int , y_int ) )+"\t" ) 

            total_num_int = \
                total_num_int + self.getNumberOfAgentsInPosition( x_int , y_int )

          else:

            grid_file.write( "0\t" )

        grid_file.write("\n")


    elif( type( output_filename_str_ ) == bool ):


      print "\nGrid Info:"

      print "-----------------------------------------------------------------------------------"

      total_num_int = 0

      for y_int in range( self.getGridSizeY() ):

        for x_int in range( self.getGridSizeX() ):

          if( self.isPositionAllowed( x_int, y_int ) ):

            sys.stdout.write( str( self.getNumberOfAgentsInPosition( x_int , y_int ) )+"\t" ) 

            total_num_int = \
                total_num_int + self.getNumberOfAgentsInPosition( x_int , y_int )

          else:

            sys.stdout.write( "0\t" )

        sys.stdout.write("\n")

      print "-----------------------------------------------------------------------------------"

      print "grid size:\n" ,"x:", self.getGridSizeX()

      print "y",self.getGridSizeY()

      print "Total number of agents:", total_num_int

      print "-----------------------------------------------------------------------------------"
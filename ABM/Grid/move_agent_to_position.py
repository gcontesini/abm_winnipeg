#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import numpy as numpy_module

class moveAgentToPosition( object ):

  def moveAgentToPosition(
    self,
    agent_obj_
  ):

    from_pos_x_int = agent_obj_.getAgentCurrentPositionX()
    from_pos_y_int = agent_obj_.getAgentCurrentPositionY()

    next_pos_x_int = agent_obj_.getAgentNextPositionX()
    next_pos_y_int = agent_obj_.getAgentNextPositionY()

    self.addAgentToPosition(
      next_pos_x_int,
      next_pos_y_int,
      agent_obj_
    )

    agent_obj_.setAgentCurrentPositionX( next_pos_x_int )
    agent_obj_.setAgentCurrentPositionY( next_pos_y_int )

    self.grid_mtx[ from_pos_x_int ][ from_pos_y_int ].remove( 
      agent_obj_
    )
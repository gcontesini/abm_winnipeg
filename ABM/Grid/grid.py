#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from grid_methods import*

class Grid(

addAgentToPosition,    
chooseRandPositionFar,
chooseRandPositionNear,
chooseRandPosition,
createGrid,
getGrid,
getGridInfo,  
getGridSizeX,  
getGridSizeY,  
getNumberOfAgentsInPosition,      
getRandomNumber,
isPositionAllowed,
moveAgentToPosition,
setGridAllowedPositions,
showAllowedPositionInGrid   

):      

# ------------------------------------------------------------------ Constructor

  def __init__( 
    self                ,
    grid_size_x_int_    ,
    grid_size_y_int_    ,
    file_name_xml_ = False
  ):

    # --------------------------------------------------------------- Attributes
    
    self.grid_size_x_int = grid_size_x_int_
    self.grid_size_y_int = grid_size_y_int_

    self.grid_mtx = None
    self.allowed_area_mtx = None

    # ------------------------------------------------------------------ Setters

    self.setGridAllowedPositions( file_name_xml_ )

    self.createGrid( )

# -------------------------------------------------------------------------- END
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from disease_methods import*

class Disease(
  decreaseDiseaseStateTime,
  getDiseaseSourceUID,
  getDiseaseSourcePlaceX,
  getDiseaseSourcePlaceY,
  getDiseaseSourceActivity,
  getDiseaseState,
  getDiseaseStateTime,
  getDiseaseTotalSickTime,
  isDiseaseDiagnosed,
  isDiseaseStateExposed,
  isDiseaseStateInfected,
  isDiseaseStateRecovered,
  isDiseaseStateSusceptible,
  isDiseaseTransmissionAble,
  setDiseaseSourceUID,
  setDiseaseSourcePlaceX,
  setDiseaseSourcePlaceY,
  setDiseaseSourceActivity,
  setDiseaseStateAsExposed,
  setDiseaseStateAsInfected,
  setDiseaseStateAsRecovered,
  setDiseaseStateAsSusceptible,
  setDiseaseStateTime,
  setDiseaseTotalSickTime,
  setDiseaseDiagnosticAsTrue,
  setDiseaseTransmissionCapabilityToDisable,
  setDiseaseTransmissionCapabilityToAble,
  setDiseaseDiagnosticAsFalse
):

  # This is an abstract class so it has no constructor {__init__() } and nether self

  disease_transmission_capability_bool = False
  disease_diagnostic_bool = None
  disease_state_char = 's'
  state_time_int = 0
  disease_source_uid_int = None
  disease_source_place_x_int = None
  disease_source_place_y_int = None
  disease_source_activity_str = None

    # self.increase_risk_int
    # self.protection_level_int

#----------------------------------------------------------------------------End

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

# !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# This method shloud be a Simulation Class #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

class updateAgentsSchedule( object ):

  def updateAgentsSchedule(
    self,
    agents_list_,
    simulation_counters_obj_,
    simulation_parameters_obj_
  ):

    day_of_the_week_int = simulation_counters_obj_.getDayOfTheWeek()
    hour_int_ = simulation_counters_obj_.getHour()
    simulation_hour_int = simulation_counters_obj_.getSimulationHour()

    for agent_obj in agents_list_ : 
      
      if( agent_obj.getAgentProperTime() <= simulation_hour_int ):

        if( agent_obj.getAgentActivityDuration() == 0 ):

          agent_obj.setDiseaseTransmissionCapabilityToAble()

          if( agent_obj.isDiseaseStateInfected() ):

            if( self.getRandomNumberDefault() <=\
              simulation_parameters_obj_.getProbabilityOfStayHomeSick()
            ):

              agent_obj.setAgentActivityAsHome()

              agent_obj.setAgentActivityDuration(
                int(
                  self.normalDistribution(
                    abs( 24-hour_int_ ), 0.5 
                  )
                )
              )

          # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Weekend

          elif( day_of_the_week_int == 6 or day_of_the_week_int == 7 ):

            # ------------------------------------------------------------ Babys

            if( agent_obj.getAgentWorkGroup() == None ):

              self.chooseBabyWeekendSchedule( 
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------------ Child

            elif( agent_obj.getAgentWorkGroup().getGroupType() == "day_care" ):

              self.chooseChildWeekendSchedule(
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------- Adolescent

            elif( agent_obj.getAgentWorkGroup().getGroupType() == "school" ):

              self.chooseAdolescentWeekendSchedule( 
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------------ Adult

            elif( agent_obj.getAgentWorkGroup().getGroupType() == "work" ):

              self.chooseAdultWeekendSchedule(
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------------ Elder

            elif( agent_obj.getAgentAge() >= 80 ):

              self.chooseElderWeekendSchedule( 
                agent_obj,
                hour_int_
              )

          # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Week

          else:

            # ------------------------------------------------------------- Baby

            if( agent_obj.getAgentWorkGroup() == None ):

              self.chooseBabyWeekSchedule(
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------------ Child

            elif( agent_obj.getAgentWorkGroup().getGroupType() == "day_care" ):

              self.chooseChildWeekSchedule(
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------------- Teen

            elif( agent_obj.getAgentWorkGroup().getGroupType() == "school" ):

              self.chooseAdolescentWeekSchedule(
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------------ Adult

            elif( agent_obj.getAgentWorkGroup().getGroupType() == "work" ):

              self.chooseAdultWeekSchedule(
                agent_obj,
                hour_int_
              )

            # ------------------------------------------------------------ Elder

            elif( agent_obj.getAgentAge() >= 80 ):

              self.chooseElderWeekSchedule( 
                agent_obj,
                hour_int_
              )

        elif( agent_obj.getAgentActivityDuration() > 0 ):

          agent_obj.decreaseAgentActivityDuration()
          agent_obj.setDiseaseTransmissionCapabilityToDisable()
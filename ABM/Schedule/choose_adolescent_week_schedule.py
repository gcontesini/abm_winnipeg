#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseAdolescentWeekSchedule( object ):

  def chooseAdolescentWeekSchedule(
    self,
    agent_obj_,
    hour_int_           
  ):

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home
  
    if( hour_int_ >= 0 and hour_int_ < 7 ):

      agent_obj_.setAgentActivityAsHome()
      agent_obj_.setAgentActivityDuration(
        int(
            self.normalDistribution(
              abs( 7-hour_int_), 0.5 
          )
        )
      )

      agent_obj_.setAgentNextPositionX(
        agent_obj_.getAgentFamilyGroup().getGroupPositionX()
      )
      agent_obj_.setAgentNextPositionY(
        agent_obj_.getAgentFamilyGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Random

    elif( hour_int_ >= 7 and hour_int_ < 8 ):

      agent_obj_.setAgentActivityAsRandom()

      agent_obj_.setAgentActivityDuration( 
        int(
          self.normalDistribution(
            abs( 8-hour_int_ ), 0.5 
          )
        )
      )

      pos_tpl = self.grid_obj.chooseRandPositionFar(
        agent_obj_.getAgentCurrentPositionX(),
        agent_obj_.getAgentCurrentPositionY()
      )

      agent_obj_.setAgentNextPositionX( pos_tpl[0] )
      agent_obj_.setAgentNextPositionY( pos_tpl[1] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~School

    elif( hour_int_ >= 8 and hour_int_ < 15 ):

      agent_obj_.setAgentActivityAsSchool()
      agent_obj_.setAgentActivityDuration( 
        int(
          self.normalDistribution(
            abs( 15-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj_.setAgentNextPositionX(
        agent_obj_.getAgentWorkGroup().getGroupPositionX()
      )
      agent_obj_.setAgentNextPositionY(
        agent_obj_.getAgentWorkGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Leisure-Activity

    elif( hour_int_ >= 15 and hour_int_ < 18 ):

      agent_obj_.setAgentActivityAsLeisureActivity()

      agent_obj_.setAgentActivityDuration( 
        int(
          self.normalDistribution(
            abs( 18-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj_.setAgentNextPositionX(
        agent_obj_.getAgentLeisureActivityGroup().getGroupPositionX()
      )
      agent_obj_.setAgentNextPositionY(
        agent_obj_.getAgentLeisureActivityGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Social

    elif( hour_int_ >= 18 and hour_int_ < 19 ):

      agent_obj_.setAgentActivityAsSocial()
      agent_obj_.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 19-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj_.setAgentNextPositionX(
        agent_obj_.getAgentSocialGroup().getGroupPositionX()
      )
      agent_obj_.setAgentNextPositionY(
        agent_obj_.getAgentSocialGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home

    elif( hour_int_ >= 20 ):

      agent_obj_.setAgentActivityAsHome()
      agent_obj_.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 24-hour_int_ ), 0.5 
          )
        )
      )
      agent_obj_.setAgentNextPositionX(
        agent_obj_.getAgentWorkGroup().getGroupPositionX()
      )
      agent_obj_.setAgentNextPositionY(
        agent_obj_.getAgentWorkGroup().getGroupPositionY()
      )

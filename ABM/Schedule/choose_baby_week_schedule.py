#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseBabyWeekSchedule( object ):

  def chooseBabyWeekSchedule(
    self,
    agent_obj,
    hour_int_           
  ):

    mother_agent_obj = agent_obj.getAgentFamilyGroup().getAgent(0)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home
  
    if( hour_int_ >= 0 and hour_int_ < 17 ):

      agent_obj.setAgentActivityAsHome()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 13-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentFamilyGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentFamilyGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Maternity license

      mother_agent_obj.setAgentActivityAsHome()

      mother_agent_obj.setAgentActivityDuration(
        agent_obj.getAgentActivityDuration()
      )

      mother_agent_obj.setAgentNextPositionX(
        agent_obj.getAgentNextPositionX()
      )
      
      mother_agent_obj.setAgentNextPositionY(
        agent_obj.getAgentNextPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Random

    elif( hour_int_ >= 17 and hour_int_ < 19 ):

      agent_obj.setAgentActivityAsRandom()

      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 19-hour_int_ ), 0.5 
          )
        )
      )

      pos_tpl = self.grid_obj.chooseRandPositionFar(
        agent_obj.getAgentCurrentPositionX(),
        agent_obj.getAgentCurrentPositionY()
      )

      agent_obj.setAgentNextPositionX( pos_tpl[0] )
      agent_obj.setAgentNextPositionY( pos_tpl[1] )


      #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Maternity license

      mother_agent_obj.setAgentActivityAsHome()

      mother_agent_obj.setAgentActivityDuration(
        agent_obj.getAgentActivityDuration()
      )

      mother_agent_obj.setAgentNextPositionX(
        agent_obj.getAgentNextPositionX()
      )
      
      mother_agent_obj.setAgentNextPositionY(
        agent_obj.getAgentNextPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home

    elif( hour_int_ >= 19 ):

      agent_obj.setAgentActivityAsHome()

      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 24-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentFamilyGroup().getGroupPositionX()
      )

      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentFamilyGroup().getGroupPositionY()
      )

      #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Maternity license

      mother_agent_obj.setAgentActivityAsHome()

      mother_agent_obj.setAgentActivityDuration(
        agent_obj.getAgentActivityDuration()
      )

      mother_agent_obj.setAgentNextPositionX(
        agent_obj.getAgentNextPositionX()
      )
      
      mother_agent_obj.setAgentNextPositionY(
        agent_obj.getAgentNextPositionY()
      )
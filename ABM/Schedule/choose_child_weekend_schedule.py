#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseChildWeekendSchedule( object ):

  def chooseChildWeekendSchedule(
    self,
    agent_obj,
    hour_int_           
  ):

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home
  
    if( hour_int_ >= 0 and hour_int_ < 10 ):

      agent_obj.setAgentActivityAsHome()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 10-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentFamilyGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentFamilyGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Leisure-Activity

    elif( hour_int_ >= 10 and hour_int_ < 13 ):

      agent_obj.setAgentActivityAsLeisureActivity()

      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 13-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentLeisureActivityGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentLeisureActivityGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Random

    elif( hour_int_ >= 13 and hour_int_ < 16 ):

      agent_obj.setAgentActivityAsRandom()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 16-hour_int_ ), 0.5 
          )
        )
      )

      pos_tpl = self.grid_obj.chooseRandPositionFar(
        agent_obj.getAgentCurrentPositionX(),
        agent_obj.getAgentCurrentPositionY()
      )

      agent_obj.setAgentNextPositionX( pos_tpl[0] )
      agent_obj.setAgentNextPositionY( pos_tpl[1] )


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Social

    elif( hour_int_ >= 16 and hour_int_ < 19 ):

      agent_obj.setAgentActivityAsSocial()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 19-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentSocialGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentSocialGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home

    elif( hour_int_ >= 19 ):

      agent_obj.setAgentActivityAsHome()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs(24-hour_int_), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentFamilyGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentFamilyGroup().getGroupPositionY()
      )

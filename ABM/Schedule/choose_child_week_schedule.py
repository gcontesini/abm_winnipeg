#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class chooseChildWeekSchedule( object ):

  def chooseChildWeekSchedule(
    self,
    agent_obj,
    hour_int_           
  ):

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home
  
    if( hour_int_ >= 0 and hour_int_ < 7 ):

      agent_obj.setAgentActivityAsHome()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 7-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentFamilyGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentFamilyGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Random

    elif( hour_int_ >= 7 and hour_int_ < 8 ):

      agent_obj.setAgentActivityAsRandom()

      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 8-hour_int_ ), 0.5 
          )
        )
      )

      pos_tpl = self.grid_obj.chooseRandPositionFar(
        agent_obj.getAgentCurrentPositionX(),
        agent_obj.getAgentCurrentPositionY()
      )

      agent_obj.setAgentNextPositionX( pos_tpl[0] )
      agent_obj.setAgentNextPositionY( pos_tpl[1] )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Day-Care

    elif( hour_int_ >= 8 and hour_int_ < 17 ):

      agent_obj.setAgentActivityAsDayCare()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 17-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentWorkGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentWorkGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Social

    elif( hour_int_ >= 17 and hour_int_ < 19 ):

      agent_obj.setAgentActivityAsSocial()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs( 19-hour_int_ ), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentSocialGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentSocialGroup().getGroupPositionY()
      )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Home

    elif( hour_int_ >= 19 ):

      agent_obj.setAgentActivityAsHome()
      agent_obj.setAgentActivityDuration(
        int(
          self.normalDistribution(
            abs(24-hour_int_), 0.5 
          )
        )
      )

      agent_obj.setAgentNextPositionX(
        agent_obj.getAgentFamilyGroup().getGroupPositionX()
      )
      agent_obj.setAgentNextPositionY(
        agent_obj.getAgentFamilyGroup().getGroupPositionY()
      )
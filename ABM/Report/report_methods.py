#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from close                                                            import	*
from write                                                            import	*
from write_age_distribution                                           import	*
from write_gender_distribution                                        import	*
from write_graphml                                                    import  *
from write_gml                                                        import	*
from write_adjacency_matrix                                           import  *
from write_gml_family                                                 import 	*
from write_gml_school                                                 import 	*
from write_gml_day_care                                               import 	*
from write_gml_social                                                 import 	*
from write_gml_leisure_activity                                       import 	*
from write_gml_work                                                   import	*
from write_mother_child_age_difference                                import	*
from write_agent_partner_age_difference                               import 	*
from write_household_size_distribution                                import	*
from write_day_care_group_size_distribution                           import	*
from write_school_group_size_distribution                             import	*
from write_work_group_size_distribution                               import	*
from write_population_profile                                         import	*
from write_social_group_size_distribution                             import 	*
from write_leisure_activity_group_size_distribution                   import	*
from write_marital_status_distribution                                import	*
from write_simulation_report                                          import	*
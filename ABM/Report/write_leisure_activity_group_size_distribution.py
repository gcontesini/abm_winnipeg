#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeLeisureActivityGroupSizeDistribution( object ):

  def writeLeisureActivityGroupSizeDistribution(
    self,
    grid_obj_,
    simulation_parameters_obj_
  ):

    leisure_activity_groups_list = []
    self.write("Group_size")
    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.getGrid()[ x_int ][ y_int ]:

            if(
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getLeisureActivityGroupAgeGroup()[0]
              and
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getLeisureActivityGroupAgeGroup()[1]
            ):

              if( 
                agent_obj.getAgentLeisureActivityGroup() 
                not in leisure_activity_groups_list
              ):

                leisure_activity_groups_list.append( 
                  agent_obj.getAgentLeisureActivityGroup()
                )

    for leisure_activity_obj in leisure_activity_groups_list :

      self.write(
        str( leisure_activity_obj.getNumberOfMembersInTheGroup() )+"\n"
      )
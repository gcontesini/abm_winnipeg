#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeSimulationReport( object ):

  def writeSimulationReport(
    self,
    agent_obj_,
    simulation_counters_obj_
  ):

    self.write( agent_obj_.getAgentID() )
    self.write(":")
    # self.write( agent_obj_.getAgentGender() )
    # self.write(":")
    # self.write( agent_obj_.getAgentAge() )
    # self.write(":")
    # self.write( agent_obj_.getAgentProperTime() )
    # self.write(":")
    # self.write( agent_obj_.getAgentMaritalStatus() )
    # self.write(":")
    # self.write( agent_obj_.getAgentCurrentPositionX() )
    # self.write(":")
    # self.write( agent_obj_.getAgentCurrentPositionY() )
    # self.write(":")
    # self.write( agent_obj_.getAgentActivity() )
    # self.write(":")
    # self.write( agent_obj_.getAgentActivityDuration() )
    # self.write(":")
    # self.write( agent_obj_.getDiseaseState() )
    # self.write(":")
    self.write( agent_obj_.isDiseaseDiagnosed() )
    self.write(":")
    self.write( agent_obj_.getDiseaseSourceUID() )
    self.write(":")
    # self.write( agent_obj_.getDiseaseSourcePlaceX() )
    # self.write(":")
    # self.write( agent_obj_.getDiseaseSourcePlaceY() )
    # self.write(":")
    self.write( agent_obj_.getDiseaseSourceActivity() )
    self.write(":")
    # self.write( agent_obj_.getDiseaseStateTime() )
    # self.write(":")
    # self.write( simulation_counters_obj_.getSimulationHour())
    # self.write(":")
    self.write( abs(simulation_counters_obj_.getSimulationHour()/24)+1 )
    # self.write(":")
    # self.write( simulation_counters_obj_.getHour() )
    # self.write(":")
    # self.write( simulation_counters_obj_.getDay() )
    # self.write(":")
    # self.write( simulation_counters_obj_.getDayOfTheWeek() )
    # self.write(":")
    # self.write( simulation_counters_obj_.getMonth() )
    # self.write(":")
    # self.write( simulation_counters_obj_.getYear() )
    self.write("\n")

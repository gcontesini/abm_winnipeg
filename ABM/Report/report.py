#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from report_methods import  *
import os as os

class Report(
  close                                     ,
  write                                     ,
  writeAdjacencyMatrix                      ,
  writeAgeDistribution                      ,
  writeAgentPartnerAgeDifference            ,
  writeDayCareGroupSizeDistribution         ,
  writeGenderDistribution                   ,
  writeGraphML                              ,
  writeGML                                  ,
  writeGMLDayCare                           ,
  writeGMLFamily                            ,
  writeGMLLeisureActivity                   ,
  writeGMLSchool                            ,
  writeGMLSocial                            ,
  writeGMLWork                              ,
  writeHouseholdSizeDistribution            ,
  writeLeisureActivityGroupSizeDistribution ,
  writeMaritalStatusDistribution            ,
  writeMotherChildAgeDifference             ,
  writePopulationProfile                    ,
  writeSchoolGroupSizeDistribution          ,
  writeSimulationReport                     ,
  writeSocialGroupSizeDistribution          ,
  writeWorkGroupSizeDistribution            
):  

  #------------------------------------------------------------------Constructor
  def __init__(
    self          ,
    file_name_str_
  ):

    self.report_file_workfile = \
      open( os.path.join( "Analysis/Data/", file_name_str_ ), 'w')

  #-------------------------------------------------------------------Destructor
  def __del__(self):

    self.close()


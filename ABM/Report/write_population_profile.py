#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writePopulationProfile( object ):

  def writePopulationProfile( 
    self,
    counters_obj_
  ):

    self.write("-------------------------------------------  ") 
    self.write( "\n" )
    self.write( 
      str( "Num of agents:\t\t\t" + 
        str( counters_obj_.getTotalNumOfAgents() )
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Num of family groups:\t\t" + 
        str( counters_obj_.getTotalNumOfFamilyGroups() )
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Num of Work groups:\t\t" + 
        str( counters_obj_.getTotalNumOfWorkGroups() )
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Num of Day Care groups:\t\t" + 
        str( counters_obj_.getTotalNumOfDayCareGroups() )
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Num of School groups:\t\t" + 
        str( counters_obj_.getTotalNumOfSchoolGroups() )
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Num of Social groups:\t\t" + 
        str( counters_obj_.getTotalNumOfSocialGroups() )
      )
    )    
    self.write( "\n" )
    self.write( 
      str( "Num of Leisure Activity groups: " + 
        str( counters_obj_.getTotalNumOfLeisureActivityGroups() )
      )
    )    
    self.write("\n-------------------------------------------")   
    self.write( "\n" )
    self.write( 
      str( "Percentage of male:\t\t" )
      +
      str(
        counters_obj_.getTotalNumOfMales()  *
        100. / 
        ( counters_obj_.getTotalNumOfAgents() )
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Percentage of female:\t\t" )
      +
      str( 
        counters_obj_.getTotalNumOfFemales()  *
        100. /
        ( counters_obj_.getTotalNumOfAgents() ) 
      )
    )

    self.write("\n-------------------------------------------")  
    self.write( "\n" )
    self.write( 
      str( "Percentage of Abriginal:\t" )
      +
      str(
        counters_obj_.getTotalNumOfAboriginal()*
        100. /
        ( counters_obj_.getTotalNumOfAgents() )
      )
    )
    self.write("\n-------------------------------------------")  
    norma = \
    counters_obj_.getTotalNumOfSingle() + \
    counters_obj_.getTotalNumOfMarried() + \
    counters_obj_.getTotalNumOfSeparated()
    self.write( "\n" )
    self.write( 
      str( "Percentage of single:\t\t" )
      + 
      str( 
        counters_obj_.getTotalNumOfSingle()*
        100. /
        ( norma )
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Percentage of married:\t\t" )
      + 
      str(
        counters_obj_.getTotalNumOfMarried()*
        100. /
        ( norma ) 
      )
    )
    self.write( "\n" )
    self.write( 
      str( "Percentage of separated:\t" )
      + 
      str(
        counters_obj_.getTotalNumOfSeparated()*
        100. /
        ( norma ) 
      )
    )
    self.write("\n-------------------------------------------")       
    self.write( "\n" )
    self.write( 
      str( "Percentage of kids:\t\t" )
      + 
      str(
        counters_obj_.getTotalNumOfKids()*
        100. /
        ( counters_obj_.getTotalNumOfAgents() ) 
      )
    )
    self.write( "\n" )

#----------------------------------------------------------------------------END

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeMotherChildAgeDifference( object ):

  def writeMotherChildAgeDifference( 
    self,
    grid_obj_
  ):
    self.write("Age_Diff")
    family_list = []

    # self.write( "\n" )

    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.getGrid()[ x_int ][ y_int ]:

            if( agent_obj.getAgentFamilyGroup() not in family_list ):

              family_list.append( agent_obj.getAgentFamilyGroup() )

    for family_obj in family_list :

      if( family_obj.getNumberOfMembersInTheGroup() > 2 ):

        if( family_obj.getAgent(0).getAgentMaritalStatus() == "married" ):

          for index_int in range(2, family_obj.getNumberOfMembersInTheGroup() ):

            self.write(
                str(family_obj.getAgent(0).getAgentAge() - family_obj.getAgent( index_int ).getAgentAge() )+"\n"
              )

      elif( family_obj.getAgent(0).getAgentMaritalStatus() == "separated" ):

        if( family_obj.getNumberOfMembersInTheGroup() > 1 ) :

          for index_int in range(1, family_obj.getNumberOfMembersInTheGroup() ):

            self.write(
                str( family_obj.getAgent(0).getAgentAge() - family_obj.getAgent( index_int ).getAgentAge() )+"\n"
              )
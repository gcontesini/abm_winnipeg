#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeAgentPartnerAgeDifference( object ):

  def writeAgentPartnerAgeDifference( 
    self,
    grid_obj_
  ):
    self.write("Age_Diff")
    family_list = []

    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.getGrid()[ x_int ][ y_int ]:

            if( agent_obj.getAgentMaritalStatus() == "married" ):

              if( agent_obj.getAgentFamilyGroup() not in family_list ):

                family_list.append( agent_obj.getAgentFamilyGroup() )

    for family_obj in family_list :

      self.write(
        str(family_obj.getAgent(0).getAgentAge() - family_obj.getAgent( 1 ).getAgentAge() )+"\n"
      )

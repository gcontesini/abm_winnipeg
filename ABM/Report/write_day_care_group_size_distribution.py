#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeDayCareGroupSizeDistribution( object ):

  def writeDayCareGroupSizeDistribution(
    self,
    grid_obj_,
    simulation_parameters_obj_
  ):

    daycare_list = []
    self.write("Group_size")
    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.getGrid()[ x_int ][ y_int ]:

            if(
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getDayCareGroupAgeGroup()[0]
              and 
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getDayCareGroupAgeGroup()[1]
            ):

              if( agent_obj.getAgentWorkGroup() not in daycare_list ):

                daycare_list.append( agent_obj.getAgentWorkGroup() )
    self.write("Group_size")
    for daycare_obj in daycare_list :

      self.write(
        str( daycare_obj.getNumberOfMembersInTheGroup() )+"\n"
      )
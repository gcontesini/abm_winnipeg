#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeAdjacencyMatrix( object ):

  def writeAdjacencyMatrix(
    self,
    grid_obj_,
    counter_obj_,
    simulation_parameters_obj_
  ):

    import numpy as numpy_module

    grid_mtx = numpy_module.zeros(
      (
        counter_obj_.getTotalNumOfAgents(),
        counter_obj_.getTotalNumOfAgents()
      ),
      dtype = int
    )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Grab All Groups
    social_group_list = []
    leisure_activity_group_list = []
    family_group_list = []
    work_group_list = []

    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.grid_mtx[ x_int ][ y_int ]:

            if( 
              agent_obj.getAgentFamilyGroup()  \
              not in                           \
              family_group_list
            ):

              family_group_list.append( agent_obj.getAgentFamilyGroup() )

            if(
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getWorkGroupAgeGroup()[0]
              and
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getWorkGroupAgeGroup()[1]
            ):

              if( 
                  agent_obj.getAgentWorkGroup()  \
                  not in                         \
                  work_group_list
                ):

                  work_group_list.append( agent_obj.getAgentWorkGroup() )

            if(
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getSocialGroupAgeGroup()[0]
              and
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getSocialGroupAgeGroup()[1]
            ):

              if( 
                  agent_obj.getAgentSocialGroup()   \
                  not in                            \
                  social_group_list
                ):

                  social_group_list.append( agent_obj.getAgentSocialGroup() )

            if(
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getLeisureActivityGroupAgeGroup()[0]
              and
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getLeisureActivityGroupAgeGroup()[1]
            ):

              if( 
                  agent_obj.getAgentLeisureActivityGroup()   \
                  not in                            \
                  leisure_activity_group_list
                ):

                  leisure_activity_group_list.append( 
                    agent_obj.getAgentLeisureActivityGroup()
                  )

    # ------------------------------------------------------------------- Family

    for family_group_obj in family_group_list :

      for i_int in range( family_group_obj.getNumberOfMembersInTheGroup() ) :

        for j_int in range( i_int+1,family_group_obj.getNumberOfMembersInTheGroup(),1 ) :

          agent_i_obj = family_group_obj.getAgent( i_int )

          agent_j_obj = family_group_obj.getAgent( j_int )

          grid_mtx[agent_i_obj.getAgentID()][agent_j_obj.getAgentID()] = 1
          grid_mtx[agent_j_obj.getAgentID()][agent_i_obj.getAgentID()] = 1

    # --------------------------------------------------------------------- Work

    for work_group_obj in work_group_list :

      for i_int in range( work_group_obj.getNumberOfMembersInTheGroup() ) :

        for j_int in range( i_int+1,work_group_obj.getNumberOfMembersInTheGroup(),1 ) :

          agent_i_obj = work_group_obj.getAgent( i_int )

          agent_j_obj = work_group_obj.getAgent( j_int )

          grid_mtx[agent_i_obj.getAgentID()][agent_j_obj.getAgentID()] = 1
          grid_mtx[agent_j_obj.getAgentID()][agent_i_obj.getAgentID()] = 1


    # ------------------------------------------------------------------- Social

    for social_group_obj in social_group_list :

      for i_int in range( social_group_obj.getNumberOfMembersInTheGroup() ) :

        for j_int in range( i_int+1,social_group_obj.getNumberOfMembersInTheGroup(),1 ) :

          agent_i_obj = social_group_obj.getAgent( i_int )

          agent_j_obj = social_group_obj.getAgent( j_int )

          grid_mtx[agent_i_obj.getAgentID()][agent_j_obj.getAgentID()] = 1
          grid_mtx[agent_j_obj.getAgentID()][agent_i_obj.getAgentID()] = 1

    # --------------------------------------------------------- Leisure Activity

    for leisure_activity_group_obj in leisure_activity_group_list :

      for i_int in range( leisure_activity_group_obj.getNumberOfMembersInTheGroup() ) :

        for j_int in range( i_int+1,leisure_activity_group_obj.getNumberOfMembersInTheGroup(),1 ) :

          agent_i_obj = leisure_activity_group_obj.getAgent( i_int )

          agent_j_obj = leisure_activity_group_obj.getAgent( j_int )

          grid_mtx[agent_i_obj.getAgentID()][agent_j_obj.getAgentID()] = 1
          grid_mtx[agent_j_obj.getAgentID()][agent_i_obj.getAgentID()] = 1
    

    for i_int in range(counter_obj_.getTotalNumOfAgents()):
        
        for j_int in range(counter_obj_.getTotalNumOfAgents()):

            if(j_int == counter_obj_.getTotalNumOfAgents()-1):

              self.write( str(grid_mtx[i_int][j_int]))
              self.write( "\n" ) 

            else:

              self.write( str(grid_mtx[i_int][j_int]))
              self.write( " " )

    self.write( "\n" ) 
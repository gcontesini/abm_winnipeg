#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeGMLSocial( object ):

  def writeGMLSocial(
    self,
    grid_obj_,
    simulation_parameters_obj_
  ):

    self.write( "graph [" )
    self.write( "\n" )
    self.write( "\tdirected 0" )
    self.write( "\n" )
    self.write(
      "\tlabel 'Interactions between agents at Social ( Complex System Graph ) ' "
    )
    self.write( "\n" )

    # -------------------------------------------------------------------- Nodes

    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.grid_mtx[ x_int ][ y_int ] :

            if( 
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getSocialGroupAgeGroup()[0]
              and
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getSocialGroupAgeGroup()[1]
            ):

              self.write( "\tnode [" )
              self.write( "\n" )
              self.write( "\t\tid "+\
                str(
                  agent_obj.getAgentID() 
                ) 
              )
              self.write( "\n" )
              self.write( "\t\tage "+ \
                str(
                  agent_obj.getAgentAge() 
                ) 
              )
              self.write( "\n" )
              self.write( "\t\tsocial_size "+\
                str(
                  agent_obj.
                  getAgentSocialGroup().
                  getNumberOfMembersInTheGroup() 
                )
              )
              self.write( "\n" )
              self.write( "\t]" )
              self.write( "\n" )

    social_group_list = []

    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.grid_mtx[ x_int ][ y_int ]:

            if( 
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getSocialGroupAgeGroup()[0]
              and
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getSocialGroupAgeGroup()[1]
            ):
            
              if( 
                  agent_obj.getAgentSocialGroup()   \
                  not in                            \
                  social_group_list
                ):

                  social_group_list.append( agent_obj.getAgentSocialGroup() )

    # ------------------------------------------------------------------- Social

    for social_group_obj in social_group_list :

      for i_int in range( social_group_obj.getNumberOfMembersInTheGroup() ) :

        for j_int in range( i_int+1,social_group_obj.getNumberOfMembersInTheGroup(),1 ) :

          agent_i_obj = social_group_obj.getAgent( i_int )

          agent_j_obj = social_group_obj.getAgent( j_int )

          self.write( "\tedge [" )
          self.write( "\n" )
          self.write( "\t\tsource " + str( agent_i_obj.getAgentID() ) )
          self.write( "\n" )
          self.write( "\t\ttarget " + str( agent_j_obj.getAgentID() ) )
          self.write( "\n" )
          self.write( "\t]" )
          self.write( "\n" )

    self.write( "]\n" ) 
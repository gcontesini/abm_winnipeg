#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class writeSchoolGroupSizeDistribution( object ):

  def writeSchoolGroupSizeDistribution(
    self,
    grid_obj_,
    simulation_parameters_obj_
  ):
    self.write("Group_size")
    school_list = []

    for x_int in range( grid_obj_.getGridSizeX() ):

      for y_int in range( grid_obj_.getGridSizeY() ):

        if( grid_obj_.isPositionAllowed( x_int, y_int ) ):

          for agent_obj in grid_obj_.getGrid()[ x_int ][ y_int ]:

            if(
              agent_obj.getAgentAge() >= simulation_parameters_obj_.getSchoolGroupAgeGroup()[0]
              and
              agent_obj.getAgentAge() <= simulation_parameters_obj_.getSchoolGroupAgeGroup()[1]
            ):

              if( agent_obj.getAgentWorkGroup() not in school_list ):

                school_list.append( agent_obj.getAgentWorkGroup() )

    for school_obj in school_list :

      self.write(
        str( school_obj.getNumberOfMembersInTheGroup() )+"\n"
      )
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import argparse as argparse

def commandLineParser():

  parser = argparse.ArgumentParser(
    description="Arguments Description:",
  )
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~XMl_Config_File
  parser.add_argument(
    '-p,',
    metavar = '--parameters',
    type = str,
    action = 'store',
    dest = 'simulation_parameters_file_name',
    help = 'name of the input file ( use xml ).'
  )
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Output_Name
  parser.add_argument(
    '-o,',
    metavar = '--output',
    type = str,
    action='store',
    default="run_test_1",
    dest='simulation_prefix_name',
    help='output file prefix name.'
  )
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Simulation seed
  parser.add_argument(
    '-s,',
    metavar = '--simulation_seed',
    type = int,
    action='store',
    default=100,
    dest='simulation_seed_int_',
    help='parse the number of the line where the simulation seed is stored ( default = 0).'
  )
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Infection_Probabilities
  parser.add_argument(
    '-i,',
    metavar = '--infection_probabilities',
    type = int,
    action='store',
    default=100000,
    dest='infection_probabilities_int_',
    help='parse the probability of infection.'
  )
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Probability_of_Diagnostic
  parser.add_argument(
    '-d,',
    metavar = '--probability_of_diagnostic',
    type = int,
    action='store',
    default=100000,
    dest='probability_of_diagnostic_int_',
    help='parse the probability of diagnose.'
  )
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Version
  parser.add_argument(
    '--version',
    action = 'version',
    help = __version__
  )

  return parser.parse_args()
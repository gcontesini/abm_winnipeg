#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import ABM.Report.report as report_class

def writeOutput(
  simulation_prefix_name_,
  grid_obj_,
  counters_obj_,
  simulation_parameters_obj_
):

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Age Group Size Distribution

  male_age_distribution_Report_obj = report_class.Report(
    str( simulation_prefix_name_ ) + \
    str( '_male_age_distribution.dat' )
  )

  female_age_distribution_Report_obj = report_class.Report(
    str( simulation_prefix_name_ ) + \
    str( '_female_age_distribution.dat' )
  )

  male_age_distribution_Report_obj.writeAgeDistribution(
    grid_obj_,
    0
  )

  female_age_distribution_Report_obj.writeAgeDistribution(
    grid_obj_,
    1
  )

  male_age_distribution_Report_obj.close()
  female_age_distribution_Report_obj.close() 

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Population Profile

  # adjacency_matrix_Report = report_class.Report(
  #   str(
  #     str( simulation_prefix_name_ ) + '_adjacency_matrix.dat'
  #   )
  # )

  # adjacency_matrix_Report.writeAdjacencyMatrix(
  #   grid_obj_,
  #   counters_obj_,
  #   simulation_parameters_obj_
  # )

  # adjacency_matrix_Report.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Complex NetWork

  # -------------------------------------------------------------------- General

  complex_network_Report_GraphML_file_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_complex_network.GraphML'
    )
  )

  complex_network_Report_GraphML_file_obj.writeGraphML(
    grid_obj_,
    counters_obj_,
    simulation_parameters_obj_
  )

  complex_network_Report_GraphML_file_obj.close()

  # -------------------------------------------------------------------- Graphml

  complex_network_Report_GML_file_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_complex_network.gml'
    )
  )

  complex_network_Report_GML_file_obj.writeGML(
    grid_obj_,
    counters_obj_,
    simulation_parameters_obj_
  )

  complex_network_Report_GML_file_obj.close()
  # ----------------------------------------------------------------- Adult Work

  complex_network_Report_GML_work_file_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_complex_network_work.gml'
    )
  )

  complex_network_Report_GML_work_file_obj.writeGMLWork(
    grid_obj_,
    simulation_parameters_obj_
  )

  complex_network_Report_GML_work_file_obj.close()

  # ------------------------------------------------------------------- Day Care

  complex_network_Report_GML_daycare_file_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_complex_network_day_care.gml'
    )
  )

  complex_network_Report_GML_daycare_file_obj.writeGMLDayCare(
    grid_obj_,
    simulation_parameters_obj_
  )

  complex_network_Report_GML_daycare_file_obj.close()

  # --------------------------------------------------------------------- School
  
  complex_network_Report_GML_school_file_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_complex_network_school.gml'
    )
  )

  complex_network_Report_GML_school_file_obj.writeGMLSchool(
    grid_obj_,
    simulation_parameters_obj_
  )

  complex_network_Report_GML_school_file_obj.close()

  # --------------------------------------------------------------------- Social
  
  complex_network_Report_GML_social_file_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_complex_network_social.gml'
    )
  )

  complex_network_Report_GML_social_file_obj.writeGMLSocial(
    grid_obj_,
    simulation_parameters_obj_
  )

  complex_network_Report_GML_social_file_obj.close()

  # ----------------------------------------------------------- Leisure Activity
  complex_network_Report_GML_LA_file_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_complex_network_leisure_activity.gml'
    )
  )

  complex_network_Report_GML_LA_file_obj.writeGMLLeisureActivity(
    grid_obj_,
    simulation_parameters_obj_
  )

  complex_network_Report_GML_LA_file_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Household Group Size Distribution

  household_size_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_household_size.dat'
    )
  )

  household_size_Report_obj.writeHouseholdSizeDistribution(
    grid_obj_
  )

  household_size_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Work Group Size Distribution

  work_group_size_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_) + "_work_group_size.dat"
    )
  )

  work_group_size_Report_obj.writeWorkGroupSizeDistribution(
    grid_obj_,
    simulation_parameters_obj_
  )

  work_group_size_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Day Care Group Size Distribution

  day_care_group_size_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + "_day_care_group_size.dat"
    )
  )

  day_care_group_size_Report_obj.writeDayCareGroupSizeDistribution(
    grid_obj_,
    simulation_parameters_obj_
  )

  day_care_group_size_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ School Group Size Distribution

  school_group_size_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + "_school_group_size.dat"
    )
  )

  school_group_size_Report_obj.writeSchoolGroupSizeDistribution(
    grid_obj_,
    simulation_parameters_obj_
  )

  school_group_size_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Day Care Group Size Distribution

  social_group_size_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + "_social_group_size.dat"
    )
  )

  social_group_size_Report_obj.writeSocialGroupSizeDistribution(
    grid_obj_,
    simulation_parameters_obj_
  )
  social_group_size_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Day Care Group Size Distribution

  leisure_activity_group_size_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + "_leisure_activity_group_size.dat"
    )
  )

  leisure_activity_group_size_Report_obj.\
    writeLeisureActivityGroupSizeDistribution(    
    grid_obj_,
    simulation_parameters_obj_
    )

  leisure_activity_group_size_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Mothe-Child Age Difference

  mother_child_age_distribution_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_mother_child_age_distribution.dat'
    )
  )

  mother_child_age_distribution_Report_obj.\
    writeMotherChildAgeDifference(
      grid_obj_
    )
  mother_child_age_distribution_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Mothe-Child Age Difference

  agent_partner_age_distribution_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_agent_partner_age_distribution.dat'
    )
  )

  agent_partner_age_distribution_Report_obj.\
    writeAgentPartnerAgeDifference(
      grid_obj_
    )

  agent_partner_age_distribution_Report_obj.close()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Marital Status Distribution

  marital_status_distribution_Report_obj = report_class.Report(
    str(
      str( simulation_prefix_name_ ) + '_marital_status_distribution.dat'
    )
  )

  marital_status_distribution_Report_obj.\
    writeMaritalStatusDistribution(
      grid_obj_
    )
    
  marital_status_distribution_Report_obj.close()

  # ------------------------------------------------------------------------ END
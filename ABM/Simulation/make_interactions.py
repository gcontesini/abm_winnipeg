#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class makeInteractions(object):

  def makeInteractions(
    self,
    agent_list_,
    simulation_counters_obj_,
    simulation_parameters_obj_,
    simulation_report_obj_,
    grid_obj_
  ):

    for agent_obj in agent_list_:

      if( agent_obj.isDiseaseStateInfected() ):
        
        if( agent_obj.isDiseaseTransmissionAble() ):
          
          if( agent_obj.isAgentAtWork() ):
            
            self.tryToInfectWorkGroup(
              agent_obj,
              simulation_parameters_obj_,
              simulation_counters_obj_
            )

          elif( agent_obj.isAgentAtHome() ):
            
            self.tryToInfectHomeGroup(
              agent_obj,
              simulation_parameters_obj_,
              simulation_counters_obj_
            )

          elif( agent_obj.isAgentAtSocial()):

            self.tryToInfectSocialGroup(
              agent_obj,
              simulation_parameters_obj_,
              simulation_counters_obj_
              )

          elif( agent_obj.isAgentAtSchool() ):
            
            self.tryToInfectSchoolGroup(
              agent_obj,
              simulation_parameters_obj_,
              simulation_counters_obj_
            )

          elif( agent_obj.isAgentAtDayCare() ):
            
            self.tryToInfectDayCareGroup(
              agent_obj,
              simulation_parameters_obj_,
              simulation_counters_obj_
            )
            
          elif( agent_obj.isAgentAtLeisureActivity() ):

            self.tryToInfectLeisureActivityGroup(
              agent_obj,
              simulation_parameters_obj_,
              simulation_counters_obj_
            )

          elif( agent_obj.isAgentAtRandom() ):

            self.tryToInfectRandomGroup(
              agent_obj,
              simulation_parameters_obj_,
              simulation_counters_obj_,
              grid_obj_
            )

        self.tryToRecover(
          agent_obj,
          simulation_counters_obj_,
          simulation_parameters_obj_
        )

      elif( agent_obj.isDiseaseStateExposed() ):

        self.tryToRecover(
          agent_obj,
          simulation_counters_obj_,
          simulation_parameters_obj_
        )

      simulation_report_obj_.writeSimulationReport(
        agent_obj,
        simulation_counters_obj_
      )
#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class moveAgents( object ):

  def moveAgents(
    self,
    agents_list_,
    simulation_counters_obj_
  ):

    list_size_int = len( agents_list_ )

    simulation_hour_int = simulation_counters_obj_.getSimulationHour()

    for agent_obj in agents_list_:

      if( agent_obj.getAgentProperTime() < simulation_hour_int ):

        agent_obj.increaseAgentProperTime()
        
        if( 
          agent_obj.getAgentCurrentPositionX() !=\
          agent_obj.getAgentNextPositionX() and
          agent_obj.getAgentCurrentPositionY() !=\
          agent_obj.getAgentNextPositionY()
        ):

          self.grid_obj.moveAgentToPosition( agent_obj )
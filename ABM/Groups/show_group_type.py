#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

class showGroupType( object ):

  def showGroupType( self ):

    if( self.getGroupType() == "family" ):

      return "family"

    elif( self.getGroupType() == "work" ):

      return "work"

    elif( self.getGroupType() == "school" ):

      return "school"

    elif( self.getGroupType() == "day_care" ):

      return "day_care"

    elif( self.getGroupType() == "social" ):

      return "social"

    elif( self.getGroupType() == "leisure_activity" ):

      return "leisure_activity"

    else:
    	
      return False
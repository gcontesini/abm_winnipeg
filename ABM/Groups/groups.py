#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

from group_methods import*

class Groups( 

addAgentToGroup, 
getAgent,
getGroupID,
getGroupPositionX, 
getGroupPositionY, 
getGroupSize,
getGroupType,
getNumberOfMembersInTheGroup, 
removeAgentFromGroup, 
setGroupAsDayCare, 
setGroupAsFamily, 
setGroupAsSchool,
setGroupAsSocial,
setGroupAsLeisureActivity,
setGroupAsWork,
setGroupPositionX, 
setGroupPositionY, 
showGroupType                       

):

  def __init__( 
    self,
    group_id_int_
  ):
  
    # Grid Related

    self.position_x_int = None
    self.position_y_int = None

    # Group attributes

    self.group_id_int = group_id_int_
    self.group_type_str = None

    self.group_members_list = []

#----------------------------------------------------------------------------End
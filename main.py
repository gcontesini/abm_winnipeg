#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014" + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import os

from ABM.Main.main_methods import *

# ------------------------------------------------------------ " Main_Function "

def main():

  command_line_arguments = command_line_parser_function.commandLineParser()
  
  # ------------------------------------------------- " Command Line Arguments "

  simulation_parameters_obj = \
    simulationParameters_class.SimulationParameters(
      command_line_arguments.simulation_parameters_file_name
    )

  simulation_parameters_obj.setProbabilityOfInfection(
    command_line_arguments.infection_probabilities_int_
  )
  
  simulation_parameters_obj.setProbabilityOfDiseaseDiagnostic(
    command_line_arguments.probability_of_diagnostic_int_
  )

  # ---------------------------------------------------- " Xml Parameters Read "

  grid_obj = grid_class.Grid(
    simulation_parameters_obj.getGridSizeX(),
    simulation_parameters_obj.getGridSizeY(),
    command_line_arguments.simulation_parameters_file_name
  )

  # ----------------------------------------------------------- " Grid Created "

  simulation_counters_obj = \
    simulation_counters_class.SimulationCounters()

  simulation_counters_obj.setSimulationDuration(
    simulation_parameters_obj.getSimulationDuration()
  )

  # -------------------------------------------- " Simulation Counters Created "

  demographic_counters_obj = \
    demographic_counters_class.DemographicCounters(
      simulation_parameters_obj
    )

  # ------------------------------------------- " Demographic Counters Created "

  population_class.Population(
    simulation_parameters_obj,
    demographic_counters_obj,
    grid_obj
  )

  # ----------------------------------------------------- " Population Created "

  # write_ouput_method.writeOutput(
  #   command_line_arguments.simulation_prefix_name,
  #   grid_obj,
  #   demographic_counters_obj,
  #   simulation_parameters_obj
  # )

  # -------------------------------------------------------- " Reports Created "
  
  simulation_obj = simulation_class.Simulation(
    grid_obj,
    simulation_parameters_obj
  )

  for i_int in range( demographic_counters_obj.getTotalNumOfAgents() ):

    simulation_counters_obj.increaseNumberOfSusceptible()

  # ------------------------------------------------------- " Start Simulation "
  simulation_obj.chooseInitialCases(
    simulation_counters_obj,
    demographic_counters_obj,
    simulation_parameters_obj
  )

  compartmental_model_report_obj = report_class.Report(
    str( command_line_arguments.simulation_prefix_name ) + \
    str( '_compartmental_model_report.dat' )
  )

  simulation_report_obj = report_class.Report(
    str( command_line_arguments.simulation_prefix_name ) + \
    str( '_simulation_report_' ) +\
    str(simulation_parameters_obj.getProbabilityOfInfection())+\
    str("_")+\
    str(simulation_parameters_obj.getProbabilityOfDiseaseDiagnostic())+\
    str(".dat")
  )

  simulation_report_obj.write(
'''\
id:\
disease_diagnostic:\
disease_source_uid:\
disease_source_activity:\
simulation_day\
'''
  )

  simulation_report_obj.write("\n")

  while(
    simulation_counters_obj.getSimulationDay() <=\
    simulation_parameters_obj.getSimulationDuration()
    and
    simulation_counters_obj.getNumberOfSusceptible() +\
    simulation_counters_obj.getNumberOfRecovered() !=\
    demographic_counters_obj.getTotalNumOfAgents()
  ):

    for hour_int in range( 24 ):

      for x_int in range( grid_obj.getGridSizeX() ):

        for y_int in range( grid_obj.getGridSizeY() ):

          if( grid_obj.isPositionAllowed( x_int, y_int ) ):

            agents_list = grid_obj.getGrid()[ x_int ][ y_int ]

            simulation_obj.updateAgentsSchedule(
              agents_list,
              simulation_counters_obj,
              simulation_parameters_obj
            )

            simulation_obj.makeInteractions(
              agents_list,
              simulation_counters_obj,
              simulation_parameters_obj,
              simulation_report_obj,
              grid_obj
            )

            simulation_obj.moveAgents(
              agents_list,
              simulation_counters_obj
            )

      simulation_counters_obj.increaseHour()
      simulation_counters_obj.increaseSimulationHour()

  simulation_report_obj.close()

  # --------------------------------------------------------- " Simulation End "

if __name__ == "__main__":

  main()

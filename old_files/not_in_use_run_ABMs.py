#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM"
__version__   = "0.0.1"

import os 
# import argparse
import time
import sys

#------------------------------------------------------------------Main_Function

def main():

  # parser = argparse.ArgumentParser(
  #   description="Arguments Description:",
  #   epilog= '''
  #       " Are you suggesting that coconuts migrate ? "
  #       ( Guard )
  #       '''
  # )

  # parser.add_argument(
  #   '-n,',
  #   metavar = '--number',
  #   type = str,
  #   action = 'store',
  #   dest = 'number_of_times',
  #   help = 'number of times that the simulation will be executed.'
  # )


  # parser.add_argument(
  #   '--version',
  #   action = 'version',
  #   help = __version__
  # )

  # argument = parser.parse_args()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # max values 100000 min 5000
  n_int = 0

  for prob_infection_index_int in range(5000,100000,5000):
    for prob_diagnostic_int in range(5000,30000,5000):
      for home_index_int in range(20000,100000,5000):
        for work_index_int in range(5000,100000,5000):
          for random_index_int in range(5000,100000,5000):
            for social_index_int in range(5000,100000,5000):
              for leisure_index_int in range(5000,100000,5000):
                for school_index_int in range(5000,100000,5000):
                  for day_care_index_int in range(5000,100000,5000):

                    # update_progress(float(n_int/(1000000) ) )      
                    os.system(
                      "./main.py "+\
                      "-p simulation_confs.xml "+\
                      "-o run_"+str( n_int )+\
                      " -1 "+str(prob_infection_index_int)+\
                      " -2 "+str(prob_diagnostic_int)+\
                      " -3 "+str(home_index_int)+\
                      " -4 "+str(work_index_int)+\
                      " -5 "+str(random_index_int)+\
                      " -6 "+str(social_index_int)+\
                      " -7 "+str(leisure_index_int)+\
                      " -8 "+str(school_index_int)+\
                      " -9 "+str(day_care_index_int)
                    )

                    n_int = n_int +1
                    print n_int


  # update_progress() : Displays or updates a console progress bar
  ## Accepts a float between 0 and 1. Any int will be converted to a float.
  ## A value under 0 represents a 'halt'.
  ## A value at 1 or bigger represents 100%
# def update_progress(progress):
  
#   barLength = 1000 # Modify this to change the length of the progress bar
#   status = ""
#   if isinstance(progress, int):
#     progress = float(progress)
#   if not isinstance(progress, float):
#     progress = 0
#     status = "error: progress var must be float\r\n"
#   if progress < 0:
#     progress = 0
#     status = "Halt...\r\n"
#   if progress >= 1:
#     progress = 1
#     status = "Done...\r\n"
#   block = int(round(barLength*progress))
#   text = "\rPercent: [{0}] {1}% {2}".format( "="*block + " "*(barLength-block), progress*100, status)
#   sys.stdout.write(text)
#   sys.stdout.flush()

if __name__ == "__main__":

  main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import sys
sys.path.append("../")
import ABM.Agents.agents as agents_class
import ABM.Grid.grid as grid_class
import ABM.Simulation_Parameters.simulation_parameters \
  as simulationParameters_class

def main():
  
  my_grid_obj = grid_class.Grid( 
    10                          ,
    10                          
  )

  #------------------------------------------------------------------- Grid Info

  my_grid_obj.setGridAllowedPositions( )

  my_grid_obj.getGridInfo()
  
  my_agent_obj = agents_class.Agents( 1 )
  
  my_agent_obj.setAgentCurrentPositionX( 1 )
  my_agent_obj.setAgentCurrentPositionY( 1 )

  my_agent_obj.setAgentNextPositionX( 2 )
  my_agent_obj.setAgentNextPositionY( 2 )

  my_grid_obj.addAgentToPosition( 
    my_agent_obj.getAgentCurrentPositionX(),
    my_agent_obj.getAgentCurrentPositionY(),
    my_agent_obj
  )
  my_grid_obj.getGridInfo()

  print "\nAgent Position Before Moving:"
  print "x:", my_agent_obj.getAgentCurrentPositionX(), \
    "y:", my_agent_obj.getAgentCurrentPositionY()

  my_grid_obj.moveAgentToPosition(
    my_agent_obj
  )
  print "\nAgent Position After Moving:"
  print "x:", my_agent_obj.getAgentCurrentPositionX(), \
    "y:", my_agent_obj.getAgentCurrentPositionY()

  assert my_grid_obj.getNumberOfAgentsInPosition( 1, 1 ) == 0
  assert my_grid_obj.getNumberOfAgentsInPosition( 2, 2 ) == 1
  
  my_grid_obj.getGridInfo()

  # ---------------------------------------------------------- Allowed Positions
if __name__ == "__main__":

  main()
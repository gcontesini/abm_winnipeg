#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import sys
sys.path.append("../")
import ABM.Simulation_Parameters.simulation_parameters as simulationParameters_class
import ABM.Simulation_Counters.simulation_counters as simulationCounters_class

def main():

  my_simulation_parameters_obj = \
    simulationParameters_class.SimulationParameters(
      "../simulation_confs.xml"
    )

  my_simulation_counter = \
    simulationCounters_class.SimulationCounters()

  my_simulation_counter.setSimulationDuration(
    my_simulation_parameters_obj.getSimulationDuration()
  )

  print " --------------------------------------------------------- Simulation Duration "

  print my_simulation_counter.getSimulationDuration()

  print "----------------------------------------------------------- Calendar"

  while( my_simulation_counter.getSimulationDay() < my_simulation_counter.getSimulationDuration() ):

    for hours_int in range( 24 ):

      if( my_simulation_counter.getHour() == 0 ):

        pass
        # sys.stdout.write("\n"+ str( my_simulation_counter.getDay() )+ "/"+ str( my_simulation_counter.getMonth())+ "/"+ str( my_simulation_counter.getYear() )+"\n" )

      # sys.stdout.write( str( my_simulation_counter.getHour() ) + " " )
      # my_simulation_counter.increaseHour()
    print "Day of the week:",my_simulation_counter.getDayOfTheWeek()
    my_simulation_counter.increaseDay()
    
  # print "\n\nTotal Number of Simulation Days:",my_simulation_counter.getSimulationDay()

  print "\n"

  print "----------------------------------------------------------- Disease Related "

  print "\nStart:"
  print "\tSusceptible:",my_simulation_counter.getNumOfSusceptible()
  print "\tExposed:",my_simulation_counter.getNumOfExposed()
  print "\tInfected:",my_simulation_counter.getNumOfInfected()
  print "\tRecovered:",my_simulation_counter.getNumOfRecovered()

  my_simulation_counter.increaseNumOfSusceptible()

  my_simulation_counter.increaseNumOfExposed()
  my_simulation_counter.increaseNumOfExposed()

  my_simulation_counter.increaseNumOfInfected()
  my_simulation_counter.increaseNumOfInfected()
  my_simulation_counter.increaseNumOfInfected()

  my_simulation_counter.increaseNumOfRecovered()
  my_simulation_counter.increaseNumOfRecovered()
  my_simulation_counter.increaseNumOfRecovered()
  my_simulation_counter.increaseNumOfRecovered()

  print "\nAfter increased:"
  print "\tSusceptible:",my_simulation_counter.getNumOfSusceptible()
  print "\tExposed:",my_simulation_counter.getNumOfExposed()
  print "\tInfected:",my_simulation_counter.getNumOfInfected()
  print "\tRecovered:",my_simulation_counter.getNumOfRecovered()

  my_simulation_counter.decreaseNumOfSusceptible()

  my_simulation_counter.decreaseNumOfExposed()
  my_simulation_counter.decreaseNumOfExposed()

  my_simulation_counter.decreaseNumOfInfected()
  my_simulation_counter.decreaseNumOfInfected()
  my_simulation_counter.decreaseNumOfInfected()

  my_simulation_counter.decreaseNumOfRecovered()
  my_simulation_counter.decreaseNumOfRecovered()
  my_simulation_counter.decreaseNumOfRecovered()
  my_simulation_counter.decreaseNumOfRecovered()

  print "Final:"
  print "\tSusceptible:",my_simulation_counter.getNumOfSusceptible()
  print "\tExposed:",my_simulation_counter.getNumOfExposed()
  print "\tInfected:",my_simulation_counter.getNumOfInfected()
  print "\tRecovered:",my_simulation_counter.getNumOfRecovered()

if __name__ == "__main__":    

  main()
  print "\n"
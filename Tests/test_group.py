#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import sys
sys.path.append("../")
import ABM.Agents.agents as agents_class
import ABM.Groups.groups as group_class

def main():

  agent_1_obj = agents_class.Agents( 1 )
  agent_2_obj = agents_class.Agents( 2 )

  work_group_obj = group_class.Groups( 1 )
  family_group_obj = group_class.Groups( 2 )
  school_group_obj = group_class.Groups( 3 )
  day_care_group_obj = group_class.Groups( 4 )
  social_group_obj = group_class.Groups( 5 )
  leisure_activity_group_obj = group_class.Groups( 6 )

  # ----------------------------------------------------------------- Group Type

  work_group_obj.setGroupAsWork()
  family_group_obj.setGroupAsFamily()
  school_group_obj.setGroupAsSchool()
  day_care_group_obj.setGroupAsDayCare()
  social_group_obj.setGroupAsSocial()
  leisure_activity_group_obj.setGroupAsLeisureActivity()

  # ------------------------------------------------------------------- Group_Id

  print "\nGroup Id"

  print "\tFamily group id:"    , family_group_obj.getGroupID()
  print "\tWork group id:"      , work_group_obj.getGroupID()
  print "\tSchool group id:"    , school_group_obj.getGroupID()
  print "\tDay Care group id:"  , day_care_group_obj.getGroupID()
  print "\tSocial group id:"    , social_group_obj.getGroupID()
  print "\tLeisure Activity group id:"      , leisure_activity_group_obj.getGroupID()

  # ------------------------------------------------------------------- Position

  print "\nPosition X & Y"
  work_group_obj.setGroupPositionX( 10 )
  assert type( work_group_obj.getGroupPositionX() ) == int
  print "\twork group position X:", work_group_obj.getGroupPositionX()

  work_group_obj.setGroupPositionY( 3 )
  assert type( work_group_obj.getGroupPositionY() )== int
  print "\twork group position Y:", work_group_obj.getGroupPositionY()

  family_group_obj.setGroupPositionX( 1 )
  family_group_obj.setGroupPositionY( 1 )

  # ----------------------------------------------------------------- Group_Type

  print "\nGroup Type"
  print "\t\nWork"
  print "\tthis group is:", work_group_obj.showGroupType()

  print "\t\nFamily"
  print "\tthis group is:", family_group_obj.showGroupType()

  print "\t\nSchool"
  print "\tthis group is:", school_group_obj.showGroupType()

  print "\t\nDay Care"
  print "\tthis group is:", day_care_group_obj.showGroupType()

  print "\t\nSocial"
  print "\tthis group is:", social_group_obj.showGroupType()      

  print "\t\nLeisure Activity"
  print "\tthis group is:", leisure_activity_group_obj.showGroupType()

  # ------------------------------------------------------ Group Members Methods

  print "\nGroup Members Methods"
  print "\tNumber of Agents in group:", work_group_obj.getNumberOfMembersInTheGroup()

  print "\n",type( agent_1_obj )
  print "\nAdd an Agent"
  work_group_obj.addAgentToGroup( agent_1_obj )

  print "\tNumber of Agents in group:", work_group_obj.getNumberOfMembersInTheGroup()

  for index_int in range( work_group_obj.getNumberOfMembersInTheGroup() ):
    fake_agent_obj = work_group_obj.getAgent( index_int )
    print "\tagent ID:",fake_agent_obj.getAgentID(), fake_agent_obj

  print "\nAdding an secound Agent"
  work_group_obj.addAgentToGroup( agent_2_obj )

  print "\tNumber of Agents in group:", work_group_obj.getNumberOfMembersInTheGroup()
  for index_int in range( work_group_obj.getNumberOfMembersInTheGroup() ):
    fake_agent_obj = work_group_obj.getAgent( index_int )
    print "\t\tagent ID:",fake_agent_obj.getAgentID(), fake_agent_obj

  print "\nRemoving an Agent"
  work_group_obj.removeAgentFromGroup( 0 )

  print "\tNumber of Agents in group:", work_group_obj.getNumberOfMembersInTheGroup()
  for index_int in range( work_group_obj.getNumberOfMembersInTheGroup() ):
    fake_agent_obj = work_group_obj.getAgent( index_int )
    print "\t\tagent ID:",fake_agent_obj.getAgentID(), fake_agent_obj

  # --------------------------------------------------------- Agents_With_Groups

  print "\nWork Group inside agent"
  agent_1_obj.addWorkGroupToAgent( work_group_obj )

  print agent_1_obj.getAgentWorkGroup(), type(agent_1_obj.getAgentWorkGroup() )
  print agent_1_obj.getAgentWorkGroup().showGroupType()
  print "\twork group position x:",agent_1_obj.getAgentWorkGroup().getGroupPositionX()
  print "\twork group position y:",agent_1_obj.getAgentWorkGroup().getGroupPositionY()

  print "\t\nFamily Group inside agent"

  agent_1_obj.addFamilyGroupToAgent( family_group_obj )
  print agent_1_obj.getAgentFamilyGroup(), type( agent_1_obj.getAgentFamilyGroup() )
  print agent_1_obj.getAgentFamilyGroup().showGroupType()
  print "\tfamily group position x:", agent_1_obj.getAgentFamilyGroup().getGroupPositionX()
  print "\tfamily group position y:", agent_1_obj.getAgentFamilyGroup().getGroupPositionY()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Melhorar esta parte !!

  print "\nGroup manipulation inside an agent:"

  agent_2_obj.addFamilyGroupToAgent( family_group_obj )
  print "\tFamily Group:" , agent_2_obj.getAgentFamilyGroup().showGroupType()
  
  agent_2_obj.addWorkGroupToAgent( work_group_obj )
  print "\tWork Group:" , agent_2_obj.getAgentWorkGroup().showGroupType()
  
  agent_2_obj.addWorkGroupToAgent( school_group_obj )
  print "\tSchool Group:" , agent_2_obj.getAgentWorkGroup().showGroupType()
  
  agent_2_obj.addWorkGroupToAgent( day_care_group_obj )
  print "\tDay Care Group:" , agent_2_obj.getAgentWorkGroup().showGroupType()

  agent_2_obj.addSocialGroupToAgent( social_group_obj )
  print "\tSocial Group:" , agent_2_obj.getAgentSocialGroup().showGroupType()

  agent_2_obj.addLeisureActivityGroupToAgent( leisure_activity_group_obj )
  print "\tLeisure Activity Group:" , agent_2_obj.getAgentLeisureActivityGroup().showGroupType()

  # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Fim da parte que precisa ser melhorada

if __name__ == "__main__":

  main()
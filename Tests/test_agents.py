#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import sys
sys.path.append("../")
import ABM.Agents.agents as agent_class
import ABM.Groups.groups as group_class

def main():

  #----------------------------------------------------------- agent && group ID

  my_agent_obj = agent_class.Agents( 69 )
  assert type( my_agent_obj.getAgentID() ) == int
  assert my_agent_obj.getAgentID() == 69
  print "Agent UID = ", my_agent_obj.getAgentID()

  #-------------------------------------------------------------- Family Related

  my_family_obj = group_class.Groups( 11 )
  assert type( my_family_obj.getGroupID() ) == int
  assert my_family_obj.getGroupID() == 11
  print "Family UID: ", my_family_obj.getGroupID()

  assert my_agent_obj.getAgentFamilyGroup() == None
  assert my_family_obj.getNumberOfMembersInTheGroup() == 0
  my_family_obj.addAgentToGroup( my_agent_obj )
  my_agent_obj.addFamilyGroupToAgent( my_family_obj )
  assert my_family_obj.getNumberOfMembersInTheGroup() == 1
  assert my_agent_obj.getAgentFamilyGroup() != None

  #--------------------------------------------------------------Disease_Related

  print "\nDisease"
  my_agent_obj.setDiseaseStateAsSusceptible()
  print "\nis agent susceptible :",my_agent_obj.isDiseaseStateSusceptible()
  my_agent_obj.setDiseaseStateAsExposed()
  print "is agent exposed :",my_agent_obj.isDiseaseStateExposed()
  my_agent_obj.setDiseaseStateAsInfected()
  print "is agent infected :",my_agent_obj.isDiseaseStateInfected()
  my_agent_obj.setDiseaseStateAsRecovered()
  print "is agent recovered :",my_agent_obj.isDiseaseStateRecovered()

  my_agent_obj.setDiseaseStateAsExposed()
  my_agent_obj.setDiseaseStateTime( 10 )
  print "\nState time set to 10 hours."
  for i_int in range( 4 ):
    my_agent_obj.decreaseDiseaseStateTime( )

  print "disease time after 4 hours :", my_agent_obj.getDiseaseStateTime( )
  my_agent_obj.setDiseaseStateTime( 15 )
  my_agent_obj.setDiseaseStateAsInfected()
  print "agent was exposed for 10 hours now he is infected for 15 hours so total sick time will be :",\
  my_agent_obj.getDiseaseTotalSickTime( )

  my_agent_obj.setDiseaseSource( 2 )
  print "\nagent( uid = ",my_agent_obj.getAgentID()," ) infected by agent( uid = 2 )"
  print "who infected agent( uid = ",my_agent_obj.getAgentID()," ) , agent ( uid = ",my_agent_obj.getDiseaseSource(), " )"

  #--------------------------------------------------------------Agent_Ethnicity

  print "\nEthnicity"
  my_agent_obj.setAgentAsAboriginal()
  assert my_agent_obj.getAgentEthnicity() == True
  print "Agent is:", my_agent_obj.showAgentEthnicity()

  my_agent_obj.setAgentAsNonAboriginal()
  assert my_agent_obj.getAgentEthnicity() == False
  print "Agent is:", my_agent_obj.showAgentEthnicity()

  #-----------------------------------------------------------------Agent_Gender

  print "\nGender"
  my_agent_obj.setAgentAsMale()
  assert my_agent_obj.showAgentGender() == "male"
  assert my_agent_obj.getAgentGender() == 0

  print "Agent Gender [male 0] = ", \
    my_agent_obj.showAgentGender(), my_agent_obj.getAgentGender()

  my_agent_obj.setAgentAsFemale()
  assert my_agent_obj.showAgentGender() == "female"
  assert my_agent_obj.getAgentGender() == 1

  print "Agent Gender [female 1] = ", \
    my_agent_obj.showAgentGender(), my_agent_obj.getAgentGender()

  #------------------------------------------------------------Agent_Proper_Time

  print "\nProper Time"
  print "Agent proper time is:", my_agent_obj.getAgentProperTime()
  my_agent_obj.increaseAgentProperTime()
  print "Agent proper time increased"
  print "Agent proper time is:", my_agent_obj.getAgentProperTime()
  assert type( my_agent_obj.getAgentProperTime() ) == int

  #--------------------------------------------------------------------Agent_Age

  print "\nAge"
  my_agent_obj.setAgentAge( 10 )
  print "Agent age is:", my_agent_obj.getAgentAge()

  #---------------------------------------------------------Agent_Marital_Status

  print "\nMarital Status"
  print "\nSingle"
  my_agent_obj.setAgentAsSingle()
  assert my_agent_obj.getAgentMaritalStatus() == "single"
  print "Agent is:", my_agent_obj.showAgentMaritalStatus()

  print "\nMarried"
  my_agent_obj.setAgentAsMarried()
  assert my_agent_obj.getAgentMaritalStatus() == "married"
  print "Agent is:", my_agent_obj.showAgentMaritalStatus()

  print "\nSeparated"
  my_agent_obj.setAgentAsSeparated()
  assert my_agent_obj.getAgentMaritalStatus() == "separated"
  print "Agent is:", my_agent_obj.showAgentMaritalStatus()

  print "\nKid"
  my_agent_obj.setAgentAsKid()
  assert my_agent_obj.getAgentMaritalStatus() == "kid"
  print "Agent is:", my_agent_obj.showAgentMaritalStatus()

  # --------------------------------------------------------------- Grid Related

  print "\nGrid related:"
  print "Current Pos x:", my_agent_obj.getAgentFamilyGroup().getGroupPositionX()
  print "Current Pos y:", my_agent_obj.getAgentFamilyGroup().getGroupPositionY()
  print "Next Pos x:", my_agent_obj.getAgentNextPositionX()
  print "Next Pos y:", my_agent_obj.getAgentNextPositionY()
  print "\nAfter setting:\n"
  my_agent_obj.setAgentCurrentPositionX( 1 )
  my_agent_obj.setAgentCurrentPositionY( 1 )
  my_agent_obj.setAgentNextPositionX( 2 )
  my_agent_obj.setAgentNextPositionY( 2 )
  print "Current Pos x:", my_agent_obj.getAgentCurrentPositionX()
  print "Current Pos y:", my_agent_obj.getAgentCurrentPositionY()
  print "Next Pos x:", my_agent_obj.getAgentNextPositionX()
  print "Next Pos y:", my_agent_obj.getAgentNextPositionY()


  #---------------------------------------------------------- Household_Position
  print "\nFamily group:"

  assert my_agent_obj.getAgentFamilyGroup().getGroupPositionX() == None
  my_agent_obj.getAgentFamilyGroup().setGroupPositionX( 10 )
  print "Household position X:",my_agent_obj.getAgentFamilyGroup().getGroupPositionX()
  assert my_agent_obj.getAgentFamilyGroup().getGroupPositionY() == None
  my_agent_obj.getAgentFamilyGroup().setGroupPositionY( 5 )
  print "Household position Y:",my_agent_obj.getAgentFamilyGroup().getGroupPositionY()

  #--------------------------------------------------- Activity_Duration_Related

  print "\nAgent Activity Related:\n"

  for hour_int in range( 24 ):

    if( my_agent_obj.getAgentFamilyGroup().getGroupType() == None ):

      if( my_agent_obj.getAgentActivityDuration() <= 0 ):

        if( hour_int >= 0  ):

          my_agent_obj.setAgentActivityAsHome()
          my_agent_obj.setAgentActivityDuration( 7 )

        if( hour_int >= 7 ):

          my_agent_obj.setAgentActivityAsRandom()
          my_agent_obj.setAgentActivityDuration( 1 )

        if( hour_int >= 8 ):

          my_agent_obj.setAgentActivityAsWork()
          my_agent_obj.setAgentActivityDuration( 4 )

        if( hour_int >= 12 ):

          my_agent_obj.setAgentActivityAsRandom()
          my_agent_obj.setAgentActivityDuration( 1 )

        if( hour_int >= 13 ):

          my_agent_obj.setAgentActivityAsWork()
          my_agent_obj.setAgentActivityDuration( 4 )

        if( hour_int >= 17 ):

          my_agent_obj.setAgentActivityAsLeisureActivity()
          my_agent_obj.setAgentActivityDuration( 4 )

        if( hour_int >= 20 ):

          my_agent_obj.setAgentActivityAsHome()
          my_agent_obj.setAgentActivityDuration( 4 )

      print my_agent_obj.getAgentActivity(), my_agent_obj.getAgentActivityDuration(),hour_int
      my_agent_obj.decreaseAgentActivityDuration()

#-------------------------------------------------------------------- Diagnostic

    print "\n"
    print "Standart:", my_agent_obj.isDiseaseDiagnosed()
    my_agent_obj.setDiseaseDiagnosticAsTrue()
    print "After True Diagnosed ", my_agent_obj.isDiseaseDiagnosed()
    my_agent_obj.setDiseaseDiagnosticAsFalse()
    print "After False Diagnosed ", my_agent_obj.isDiseaseDiagnosed()
    print "\n"

if __name__ == "__main__":

  main()

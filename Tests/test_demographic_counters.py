#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import sys
sys.path.append("../")
import ABM.Simulation_Parameters.simulation_parameters as simulationParameters_class
import ABM.Demographic_Counters.demographic_counters as demographicCounters_class

def main():

	my_simulation_parameters_obj = \
		simulationParameters_class.SimulationParameters( "../simulation_confs.xml" )

	my_counter = demographicCounters_class.DemographicCounters( my_simulation_parameters_obj )

	print "--------------------------------------Num_of_Agents_Per_Age_Group"

	for gender_int in range( 2 ):
		print "\n",gender_int,"\n"
		for index_int in range( 18 ):
			print my_counter.getMaxNumOfAgentsForAgeGroup( gender_int, index_int )
	print "\n"

	for i_int in range( my_counter.getMaxNumOfAgentsForAgeGroup( 0, 5 ) ):
		my_counter.decreaseNumOfAgentsForAgeGroup( 0, 5 )

	for gender_int in range( 2 ):
		print "\n",gender_int,"\n"
		for index_int in range( 18 ):
			print my_counter.getMaxNumOfAgentsForAgeGroup( gender_int, index_int )
	print "\n"

	print "---------------------------------------Max Num Of Family Groups per Size"

	print "Household Size One:",my_counter.getMaxNumOfHouseholdSizeAllowed(0)
	print "Household Size Two:",my_counter.getMaxNumOfHouseholdSizeAllowed(1)
	print "Household Size Three:",my_counter.getMaxNumOfHouseholdSizeAllowed(2)
	print "Household Size Four:",my_counter.getMaxNumOfHouseholdSizeAllowed(3)
	print "Household Size Six:",my_counter.getMaxNumOfHouseholdSizeAllowed(4)
	print "Total Number of Household Groups:", 	\
		my_counter.getMaxNumOfHouseholdSizeAllowed(0) + \
		my_counter.getMaxNumOfHouseholdSizeAllowed(1) + \
		my_counter.getMaxNumOfHouseholdSizeAllowed(2) + \
		my_counter.getMaxNumOfHouseholdSizeAllowed(3) + \
		my_counter.getMaxNumOfHouseholdSizeAllowed(4)

	print "----------------------------------------------------Num_of_Agents"
	print "Num of Agents:",	my_counter.getTotalNumOfAgents()
	my_counter.increaseTotalNumOfAgents()
	print "+1"
	print "Num of Agents after increase:",	my_counter.getTotalNumOfAgents()
	my_counter.decreaseTotalNumOfAgents()
	print "-1"
	print "Num of Agents after decrease:",	my_counter.getTotalNumOfAgents()

	print "----------------------------------------------------Num_of_Work_Groups"
	print "Num of Work Groups", 	my_counter.getTotalNumOfWorkGroups()
	my_counter.increaseTotalNumOfWorkGroups()
	my_counter.increaseTotalNumOfWorkGroups()
	my_counter.increaseTotalNumOfWorkGroups()
	my_counter.increaseTotalNumOfWorkGroups()
	my_counter.increaseTotalNumOfWorkGroups()
	print "+5"
	print "Num of Work Groups:",	my_counter.getTotalNumOfWorkGroups()
	my_counter.decreaseTotalNumOfWorkGroups()
	print "-1"
	print "Num of Work Groups decrease:", 	my_counter.getTotalNumOfWorkGroups()

	print "----------------------------------------------------Num_of_Family_Groups"
	print "Num of Family Groups:",	my_counter.getTotalNumOfFamilyGroups()
	my_counter.increaseTotalNumOfFamilyGroups()
	my_counter.increaseTotalNumOfFamilyGroups()
	print "+2"
	print "Num of Family Groups:",	my_counter.getTotalNumOfFamilyGroups()
	my_counter.decreaseTotalNumOfFamilyGroups()
	print "-1"
	print "Num of Family Groups decrease:",	my_counter.getTotalNumOfFamilyGroups()

	print "----------------------------------------------------Num_of_School_Groups"
	print "Num of School Groups:",	my_counter.getTotalNumOfSchoolGroups()
	my_counter.increaseTotalNumOfSchoolGroups()
	print "+1"
	print "Num of School Groups:",	my_counter.getTotalNumOfSchoolGroups()
	my_counter.decreaseTotalNumOfSchoolGroups()
	print "-1"
	print "Num of School Groups decrease:",	my_counter.getTotalNumOfSchoolGroups()

	print "----------------------------------------------------Num_of_Day_Care_Groups"
	print "Num of DayCare Groups:",	my_counter.getTotalNumOfDayCareGroups()
	my_counter.increaseTotalNumOfDayCareGroups()
	my_counter.increaseTotalNumOfDayCareGroups()
	my_counter.increaseTotalNumOfDayCareGroups()
	print "+3"
	print "Num of DayCare Groups:",	my_counter.getTotalNumOfDayCareGroups()
	my_counter.decreaseTotalNumOfDayCareGroups()
	print "-1"
	print "Num of DayCare Groups decrease:",	my_counter.getTotalNumOfDayCareGroups()

	print "----------------------------------------------------Num_of_Social_Groups"
	print "Num of Social Groups:",	my_counter.getTotalNumOfSocialGroups()
	my_counter.increaseTotalNumOfSocialGroups()
	my_counter.increaseTotalNumOfSocialGroups()
	print "+2"
	print "Num of Social Groups:",	my_counter.getTotalNumOfSocialGroups()
	my_counter.decreaseTotalNumOfSocialGroups()
	my_counter.decreaseTotalNumOfSocialGroups()
	print "-2"
	print "Num of Social Groups decrease:",	my_counter.getTotalNumOfSocialGroups()

	print "----------------------------------------------------Num_of_Leisure_Activity_Groups"
	print "Num of Leisure Activity Groups:",	my_counter.getTotalNumOfLeisureActivityGroups()
	my_counter.increaseTotalNumOfLeisureActivityGroups()
	my_counter.increaseTotalNumOfLeisureActivityGroups()
	my_counter.increaseTotalNumOfLeisureActivityGroups()
	print "+3"
	print "Num of Leisure Activity Groups:",	my_counter.getTotalNumOfLeisureActivityGroups()
	my_counter.decreaseTotalNumOfLeisureActivityGroups()
	my_counter.decreaseTotalNumOfLeisureActivityGroups()
	my_counter.decreaseTotalNumOfLeisureActivityGroups()
	print "-3"
	print "Num of Leisure Activity Groups decrease:", my_counter.getTotalNumOfLeisureActivityGroups()

	print "-----------------------------------------------------Num_of_Males"
	print "Num of Males:",my_counter.getTotalNumOfMales()
	my_counter.increaseTotalNumOfMales()
	print "+1"
	print "Num of Males:",my_counter.getTotalNumOfMales()
	my_counter.decreaseTotalNumOfMales()
	print "-1"
	print "Num of Males:",my_counter.getTotalNumOfMales()
	print "---------------------------------------------------Num_of_Females"
	print "Num of Females:",my_counter.getTotalNumOfFemales()
	my_counter.increaseTotalNumOfFemales()
	print "+1"
	print "Num of Females:",my_counter.getTotalNumOfFemales()
	my_counter.decreaseTotalNumOfFemales()
	print "-1"
	print "Num of Females:",my_counter.getTotalNumOfFemales()
	print "--------------------------------------------------Num_of_Children"
	print "Num of Children:",my_counter.getTotalNumOfChildren()
	my_counter.increaseTotalNumOfChildren()
	print "+1"
	print "Num of Children:",my_counter.getTotalNumOfChildren()	
	my_counter.decreaseTotalNumOfChildren()
	print "-1"
	print "Num of Children:",my_counter.getTotalNumOfChildren()	
	print "-----------------------------------------------Num_of_Aboriginals"
	print "Num_of_Aboriginals:",my_counter.getTotalNumOfAboriginal()
	my_counter.increaseTotalNumOfAboriginal()
	my_counter.increaseTotalNumOfAboriginal()
	print "+2"
	print "Num_of_Aboriginals:",my_counter.getTotalNumOfAboriginal()
	my_counter.decreaseTotalNumOfAboriginal()
	my_counter.decreaseTotalNumOfAboriginal()
	print "-2"
	print "Num_of_Aboriginals:",my_counter.getTotalNumOfAboriginal()

	print "----------------------------------------------------Num_of_household"
	print "-----------------------------------------------------------------One"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 0 )
	my_counter.increaseTotalNumOfHouseholdSize( 0 )
	my_counter.increaseTotalNumOfHouseholdSize( 0 )
	print "+2"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 0 )
	my_counter.decreaseTotalNumOfHouseholdSize( 0 )
	print "-1"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 0 )
	print "-----------------------------------------------------------------Two"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 1 )
	my_counter.increaseTotalNumOfHouseholdSize( 1 )
	my_counter.increaseTotalNumOfHouseholdSize( 1 )
	print "+2"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 1 )
	my_counter.decreaseTotalNumOfHouseholdSize( 1 )
	print "-1"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 1 )
	print "----------------------------------------------------------------Three"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 2 )
	my_counter.increaseTotalNumOfHouseholdSize( 2 )
	my_counter.increaseTotalNumOfHouseholdSize( 2 )
	print "+2"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 2 )
	my_counter.decreaseTotalNumOfHouseholdSize( 2 )
	print "-1"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 2 )
	print "----------------------------------------------------------------Four"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 3 )
	my_counter.increaseTotalNumOfHouseholdSize( 3 )
	my_counter.increaseTotalNumOfHouseholdSize( 3 )
	print "+2"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 3 )
	my_counter.decreaseTotalNumOfHouseholdSize( 3 )
	print "-1"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 3 )
	print "-----------------------------------------------------------------Six"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 4 )
	my_counter.increaseTotalNumOfHouseholdSize( 4 )
	my_counter.increaseTotalNumOfHouseholdSize( 4 )
	print "+2"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 4 )
	my_counter.decreaseTotalNumOfHouseholdSize( 4 )
	print "-1"
	print "num_of_household_size_one", my_counter.getTotalNumOfHouseholdSize( 4 )

	print "----------------------------------------------------Num_of_Single"
	print "Num_of_Single:", my_counter.getTotalNumOfSingle()
	my_counter.increaseTotalNumOfSingle()
	my_counter.increaseTotalNumOfSingle()
	my_counter.increaseTotalNumOfSingle()
	print "+3"
	print "Num_of_Single:", my_counter.getTotalNumOfSingle()
	my_counter.decreaseTotalNumOfSingle()
	print "-1"
	print "Num_of_Single:", my_counter.getTotalNumOfSingle()
	print "---------------------------------------------------Num_of_Married"
	print "Num_of_Single:", my_counter.getTotalNumOfMarried() 
	my_counter.increaseTotalNumOfMarried()
	my_counter.increaseTotalNumOfMarried()
	print "+2"
	print "Num_of_Single:", my_counter.getTotalNumOfMarried()
	my_counter.decreaseTotalNumOfMarried()
	my_counter.decreaseTotalNumOfMarried()
	print "-2"
	print "Num_of_Single:", my_counter.getTotalNumOfMarried()
	print "-------------------------------------------------Num_of_Separated"
	print "Num_of_Single:", my_counter.getTotalNumOfSeparated() 
	my_counter.increaseTotalNumOfSeparated()
	my_counter.increaseTotalNumOfSeparated()
	print "+2"
	print "Num_of_Single:", my_counter.getTotalNumOfSeparated()
	my_counter.decreaseTotalNumOfSeparated()
	print "-1"
	print "Num_of_Single:", my_counter.getTotalNumOfSeparated()
	print "---------------------------------------------------Num_of_Widowed"
	print "Num_of_Widowed:", my_counter.getTotalNumOfWidowed()
	my_counter.increaseTotalNumOfWidowed()
	print "+1"
	print "Num_of_Widowed:", my_counter.getTotalNumOfWidowed()
	my_counter.decreaseTotalNumOfWidowed()
	print "-1"
	print "Num_of_Widowed:", my_counter.getTotalNumOfWidowed()
	print "--------------------------------------------------Num_of_Kids"
	print "Num_of_Kids:",my_counter.getTotalNumOfKids()
	my_counter.increaseTotalNumOfKids()
	print "+1"
	print "Num_of_Kids:",my_counter.getTotalNumOfKids()	
	my_counter.decreaseTotalNumOfKids()
	print "-1"
	print "Num_of_Kids:",my_counter.getTotalNumOfKids()

	print "---------------------------------------------------------Max_Nums"

	print "Max Nums"
	print "Percetage of Adults:", my_counter.getMaxNumOfAdultsAllowed() * \
		100. / my_simulation_parameters_obj.getPopulationSize()
	# decreaseNumOfAdultsAllowed		
	print "Percetage of Children:", my_counter.getMaxNumOfChildrenAllowed() * \
		100. / my_simulation_parameters_obj.getPopulationSize()

if __name__ == "__main__":		
	main()

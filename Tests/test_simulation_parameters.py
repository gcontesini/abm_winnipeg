#!/usr/bin/env python
# -*- coding: utf-8 -*-

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 #
#   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            #
#                                                                              #
#   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        #
#                                                                              #
#   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            #
#                                                                              #
#   All rights reserved - DO NOT REDISTRIBUTE !                                #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

__author__    = "Guilherme Contesini and Luiz C. Mostaço-Guidolin"
__copyright__ = "Copyright (C) 2014 " + __author__
__license__   = "GNU General Public License, version 3"
__program__   = "ABM_Winnipeg"
__version__   = "0.0.1"

import sys
sys.path.append("../")
import ABM.Simulation_Parameters.simulation_parameters as SimulationParameters_class

def main():

	my_simulation_parameters_obj = \
		SimulationParameters_class.SimulationParameters("../simulation_confs.xml")

	print "XML Data inside Simulation Parameters\n"
	print "\nSimulation Related Related\n"
	print "\tSimulation duration:",my_simulation_parameters_obj.getSimulationDuration()
	print "\tPopulation size:",my_simulation_parameters_obj.getPopulationSize()
	print "\tGrid size X:",my_simulation_parameters_obj.getGridSizeX()
	print "\tGrid size Y:",my_simulation_parameters_obj.getGridSizeY()
	print "\nCensus Related\n"
	print "\tProbabilitie of being Male:",my_simulation_parameters_obj.getProbabilityOfBeingMale()
	print "\tProbabilitie of being Aboriginal:",my_simulation_parameters_obj.getProbabilityOfBeingAboriginal()
	print "\tProbabilitie of being single:",my_simulation_parameters_obj.getProbabilityOfBeingSingle()
	print "\tProbabilitie of being Married:",my_simulation_parameters_obj.getProbabilityOfBeingMarried()
	print "\tProbabilitie of being Separated:", my_simulation_parameters_obj.getProbabilityOfBeingSeparated()
	print "\tProbabilitie of being Widowed:", my_simulation_parameters_obj.getProbabilityOfBeingWidowed()
	print "\tProbabilitie of Household size:", my_simulation_parameters_obj.getProbabilityOfHouseholdSize()
	print "\tProbabilitie of age group male:", my_simulation_parameters_obj.getAgeGroupProbabilitiesPerGender( 0 )
	print "\tProbabilitie of age group female:", my_simulation_parameters_obj.getAgeGroupProbabilitiesPerGender( 1 )
	print "\nGroup Related\n"
	print "\tWork Group mean:" , my_simulation_parameters_obj.getWorkGroupMean()
	print "\tWork Group sd:" , my_simulation_parameters_obj.getWorkGroupSD()
	print "\tWork Group size:" , my_simulation_parameters_obj.getWorkGroupSize()
	print "\tWork Group initial age:" , my_simulation_parameters_obj.getWorkGroupAgeGroup()[0]
	print "\tWork Group final age:" , my_simulation_parameters_obj.getWorkGroupAgeGroup()[1]
	print ""
	print "\tDayCare Group mean:" , my_simulation_parameters_obj.getDayCareGroupMean()
	print "\tDayCare Group sd:" , my_simulation_parameters_obj.getDayCareGroupSD()
	print "\tDayCare Group size:" , my_simulation_parameters_obj.getDayCareGroupSize()
	print "\tDayCare Group initial age:" , my_simulation_parameters_obj.getDayCareGroupAgeGroup()[0]
	print "\tDayCare Group final age:" , my_simulation_parameters_obj.getDayCareGroupAgeGroup()[1]
	print ""
	print "\tSchool Group mean:" , my_simulation_parameters_obj.getSchoolGroupMean()
	print "\tSchool Group sd:" , my_simulation_parameters_obj.getSchoolGroupSD()
	print "\tSchool Group size:" , my_simulation_parameters_obj.getSchoolGroupSize()
	print "\tSchool Group initial age:" , my_simulation_parameters_obj.getSchoolGroupAgeGroup()[0]
	print "\tSchool Group final age:" , my_simulation_parameters_obj.getSchoolGroupAgeGroup()[1]
	print ""
	print "\tSocial Group mean:" , my_simulation_parameters_obj.getSocialGroupMean()
	print "\tSocial Group sd:" , my_simulation_parameters_obj.getSocialGroupSD()
	print "\tSocial Group size:" , my_simulation_parameters_obj.getSocialGroupSize()
	print "\tSocial Group initial age:" , my_simulation_parameters_obj.getSocialGroupAgeGroup()[0]
	print "\tSocial Group final age:" , my_simulation_parameters_obj.getSocialGroupAgeGroup()[1]
	print ""
	print "\tLeisure Activity Group mean:" , my_simulation_parameters_obj.getLeisureActivityGroupMean()
	print "\tLeisure Activity Group sd:" , my_simulation_parameters_obj.getLeisureActivityGroupSD()
	print "\tLeisure Activity Group size:" , my_simulation_parameters_obj.getLeisureActivityGroupSize()
	print "\tLeisure Activity Group initial age:" , my_simulation_parameters_obj.getLeisureActivityGroupAgeGroup()[0]
	print "\tLeisure Activity Group final age:" , my_simulation_parameters_obj.getLeisureActivityGroupAgeGroup()[1]
	print ""
	print "\nGroup Related\n"
	print "\tAgent Partner Age Difference mean:" , my_simulation_parameters_obj.getAgentPartnerAgeDifferenceMean()
	print "\tAgent Partner Age Difference sd:" , my_simulation_parameters_obj.getAgentPartnerAgeDifferenceSD()
	print ""
	print "\tMother Child Age Difference mean:" , my_simulation_parameters_obj.getMotherChildAgeDifferenceMean()
	print "\tMother Child Age Difference sd:" , my_simulation_parameters_obj.getMotherChildAgeDifferenceSD()
	print ""
	print "\nDisease Related :\n"
	print "\tInitial Cases :",my_simulation_parameters_obj.getDiseaseInitialCases()
	print "\tDiagnostic Probability :",my_simulation_parameters_obj.getProbabilityOfDiseaseDiagnostic()
	print ""
	print "\tExposed Min Duration :",my_simulation_parameters_obj.getExposedMinDuration()
	print "\tExposed Max Duration :",my_simulation_parameters_obj.getExposedMaxDuration()
	print "\tExposed Mean :",my_simulation_parameters_obj.getExposedMean()
	print "\tExposed Sd :",my_simulation_parameters_obj.getExposedSd()
	print ""
	print "\tInfected Min Duration :",my_simulation_parameters_obj.getInfectedMinDuration()
	print "\tInfected Max Duration :",my_simulation_parameters_obj.getInfectedMaxDuration()
	print "\tInfected Mean :",my_simulation_parameters_obj.getInfectedMean()
	print "\tInfected Sd :",my_simulation_parameters_obj.getInfectedSd()
	print ""
	print "\nInteractions :\n"
	print "\tPercentage Home Interactions :",my_simulation_parameters_obj.getPercentageOfHomeInteractions()
	print "\tPercentage Work Interactions :",my_simulation_parameters_obj.getPercentageOfWorkInteractions()
	print "\tPercentage Social Interactions :",my_simulation_parameters_obj.getPercentageOfSocialInteractions()
	print "\tPercentage DayCare Interactions :",my_simulation_parameters_obj.getPercentageOfDayCareInteractions()
	print "\tPercentage School Interactions :",my_simulation_parameters_obj.getPercentageOfSchoolInteractions()
	print "\tPercentage Random Interactions :",my_simulation_parameters_obj.getPercentageOfRandomInteractions()
	print "\tPercentage Leisure Activity Interactions :",my_simulation_parameters_obj.getPercentageOfLeisureActivityInteractions()
	print ""
	print "\nProbability of Infections :\n"
	print "\tAt Home  :",my_simulation_parameters_obj.getProbabilityOfInfectionAtHome()
	print "\tAt Work  :",my_simulation_parameters_obj.getProbabilityOfInfectionAtWork()
	print "\tAt Social  :",my_simulation_parameters_obj.getProbabilityOfInfectionAtSocial()
	print "\tAt School  :",my_simulation_parameters_obj.getProbabilityOfInfectionAtSchool()
	print "\tAt DayCare  :",my_simulation_parameters_obj.getProbabilityOfInfectionAtDayCare()
	print "\tAt Leisure Activity  :",my_simulation_parameters_obj.getProbabilityOfInfectionAtLeisureActivity()
	print "\tAt Random  :",my_simulation_parameters_obj.getProbabilityOfInfectionAtRandom()
	print ""
	print "Probability of Stay Home when sick:"
	print "\tStaying Home Sick Probability:", my_simulation_parameters_obj.getProbabilityOfStayHomeSick()


if __name__ == "__main__":

	main()

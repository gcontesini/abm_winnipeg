# README #

ESTRUTURA DE UM README:

  Titulo do Projeto:

    -Descreva objetivamente oque \'e o projeto e para que serve o projeto.
    -Descreva em qual linguagem ele foi feito.
    -Outras detalhes do projeto.

  Instalacoes:

    -Onde encontrar o codigo do projeto.
    -Como instalar o Programa/Projeto/Framework. (Todas as linhas de comando).
    (Assuma que o usuario nao saiba utilizar o computador).

  Como utilizar o programa/projeto:

    -Como configurar o programa apos a instalacao.(utilize exemplos)
    -Como realizar as operacoes mais simples.(utilize exemplos)
    -Onde encontrar mais documentacao.(como encontrar o manual do projeto)

  Licenca:

    -Qual a filisofia do projeto ?
    -Qual a licenca vigente ?
    -Mostre a data do do comeco do projeto.
    -Qual a estimativa de termino do projeto:

  Contato:
  
    -Quem \'e voce ?
    -Apresente quem participa do projeto ?
    -Como encontrar em contato do os desenvolvedores.

### ODD protocol ###

## Purpose ##

Management versions of an Agent-Based Model (ABM) code.
An ABM demographic focusing on a infectious disease, in this case first and
second waves of 2009 H1N1 influenza.

## State variables and scale ##

Agents:
- age
- gender
- marital status
- ethnicity
- family group
- work group
- team group
- social group

Grid:
- Winnipeg
- 113 x 160

## Process overview and scheduling ##

To Do

## Design concepts ##

To Do

## Initialization ##

To Do

## Input ##

Reference:
  Statistics Canada, 2006 Census of Population.
  ( www.statcan.gc.ca )

How to cite:
  Statistics Canada. 2007. Winnipeg, Manitoba (Code4611040) (table).
  2006 Community Profiles. 2006 Census. Statistics Canada Catalog no. 92-591-XWE.
  Ottawa. Released March 13, 2007.
  http://www12.statcan.ca/census-recensement/2006/dp-pd/prof/92-591/index.cfm?Lang=E

## Sub models ##

To Do

#Version
0.0.1

#How to run tests
./main.py -p simulation_confs.xml -o run_*

#Deployment instructions
./main -h

### Contribution guidelines ###


   Apr. 2014 - Gulherme Contesini <gcontesini@gmail.com> && \                 \
   Mostaço-Guidolin, Luiz C. B. <lcguid@gmail.com>                            \
                                                                              \
   An Agent-Based Model to Simulate the Spread of Infectious Diseases.        \
                                                                              \
   Copyright 2014 Guilherme Contesini and Luiz C. Mostaço-Guidolin            \
                                                                              \
   All rights reserved - DO NOT REDISTRIBUTE !                                \


### Who do I talk to? ###

Guilherme Contesini (gcontesini@gmail.com or gcontesini@id.uff.br)
Luiz C. Mostaço-Guidolin Luiz C. ( lcguid@gmail.com )
